'use strict';
module.exports = (sequelize, DataTypes) => {
  const Cartobuy = sequelize.define('Cartobuy', {
    // id: {type: DataTypes.NUMBER, primaryKey: true},
    carid: DataTypes.INTEGER,
    customernumber: DataTypes.STRING,
    dateofreq: DataTypes.STRING,
    schedule: DataTypes.STRING
  }, {});
  Cartobuy.associate = function (models) {
    // associations can be defined here
  };
  return Cartobuy;
};