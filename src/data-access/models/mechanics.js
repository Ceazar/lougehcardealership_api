'use strict';
module.exports = (sequelize, DataTypes) => {
  const Mechanics = sequelize.define('Mechanics', {
    employeeid: DataTypes.STRING,
    status: DataTypes.STRING,
    // timeavailable: DataTypes.STRING,
    priority: DataTypes.INTEGER
  }, {});
  Mechanics.associate = function (models) {
    // associations can be defined here

  };
  return Mechanics;
};