'use strict';
module.exports = (sequelize, DataTypes) => {
  const Employees = sequelize.define('Employees', {
    id: { type: DataTypes.NUMBER },
    employeeid: {
      type: DataTypes.STRING,
      references: 'accounts',
      referencesKey: 'employeeid',
      primaryKey: true
    },
    fname: DataTypes.STRING,
    mi: DataTypes.STRING,
    lname: DataTypes.STRING,
    position: DataTypes.STRING,
    age: DataTypes.NUMBER,
    phone: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    freezeTableName: true
  });
  Employees.associate = function (models) {
    // associations can be defined here
    // Employees.hasOne(models.Accounts, {
    //   foreignKey: 'employeeid',
    //   targetKey: 'employeeid'
    // });

  };
  return Employees;
};