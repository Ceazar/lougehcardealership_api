'use strict';
module.exports = (sequelize, DataTypes) => {
  const Salesperson = sequelize.define('Salesperson', {
    employeeid: DataTypes.STRING,
    status: DataTypes.STRING,
    priority: DataTypes.INTEGER
  }, {});
  Salesperson.associate = function(models) {
    // associations can be defined here
  };
  return Salesperson;
};