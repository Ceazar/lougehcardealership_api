'use strict';
module.exports = (sequelize, DataTypes) => {
  const Accounts = sequelize.define('Accounts', {
    // id: {type: DataTypes.NUMBER, primaryKey: true},
    userid: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING,
  }, {
    freezeTableName: true
  });
  Accounts.associate = function (models) {
    // associations can be defined here
    // Accounts.belongsTo(models.Employees, {
    //   foreignKey: 'employeeid',
    //   targetkey: 'employeeid'
    // });
  };
  return Accounts;
};