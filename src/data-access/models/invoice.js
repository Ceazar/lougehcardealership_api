'use strict';
module.exports = (sequelize, DataTypes) => {
  const Invoice = sequelize.define('Invoice', {
    // id: {type: DataTypes.NUMBER, primaryKey: true},
    customernumber: DataTypes.STRING,
    carid: DataTypes.NUMBER,
    serviceticketnumber: DataTypes.STRING,
    date: DataTypes.STRING,
    type: DataTypes.STRING,
    totalamount: DataTypes.FLOAT
  }, {});
  Invoice.associate = function (models) {
    // associations can be defined here
  };
  return Invoice;
};