'use strict';
module.exports = (sequelize, DataTypes) => {
  const Cars = sequelize.define('Cars', {
    // id: {type: DataTypes.NUMBER, primaryKey: true},
    vin: DataTypes.STRING,
    horsepower: DataTypes.STRING,
    transmission: DataTypes.STRING,
    enginedisplacement: DataTypes.STRING,
    fueltype: DataTypes.STRING,
    model: DataTypes.STRING,
    company: DataTypes.STRING,
    color: DataTypes.STRING,
    status: DataTypes.STRING,
    price: DataTypes.FLOAT,
    available: DataTypes.BOOLEAN,
    carimage: DataTypes.STRING
  }, {});
  Cars.associate = function (models) {
    // associations can be defined here
  };
  return Cars;
};