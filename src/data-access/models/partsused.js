'use strict';
module.exports = (sequelize, DataTypes) => {
  const Partsused = sequelize.define('Partsused', {
    // id: { type: DataTypes.NUMBER, primaryKey: true },
    partid: DataTypes.NUMBER,
    servicenumber: DataTypes.STRING,
    quantity: DataTypes.NUMBER
  }, {
    freezeTableName: true
  });
  Partsused.associate = function (models) {
    // associations can be defined here
  };
  return Partsused;
};