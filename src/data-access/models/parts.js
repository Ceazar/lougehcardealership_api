'use strict';
module.exports = (sequelize, DataTypes) => {
  const Parts = sequelize.define('Parts', {
    partname: DataTypes.STRING,
    description: DataTypes.STRING,
    stock: DataTypes.NUMBER,
    price: DataTypes.FLOAT
  }, {});
  Parts.associate = function(models) {
    // associations can be defined here
  };
  return Parts;
};