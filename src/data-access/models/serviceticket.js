'use strict';
module.exports = (sequelize, DataTypes) => {
  const Serviceticket = sequelize.define('Serviceticket', {
    // id: {type: DataTypes.NUMBER, primaryKey: true},
    servicenumber: DataTypes.STRING,
    serviceid: DataTypes.INTEGER,
    customernumber: DataTypes.STRING,
    dateofappt: DataTypes.STRING,
    starttime: DataTypes.STRING,
    endtime: DataTypes.STRING,
    status: DataTypes.STRING,
    servicefee: DataTypes.FLOAT
  }, {
    // freezeTableName:true
  });
  Serviceticket.associate = function (models) {
    // associations can be defined here
    // Serviceticket.belongsTo(models.Employees, {

    //   // through: 'employeeid', 
    //   foreignKey: 'employeeid',
    //   // targetKey: 'employeeid'
    // });
    // Serviceticket.belongsTo(models.Customers, {
    //   foreignKey: 'customernumber',
    //   // targetKey: 'id',

    // });

    // Serviceticket.hasOne(models.servicerendered, {
    //   foreignKey: 'servicenumber' ,

    // })

  };
  return Serviceticket;
};