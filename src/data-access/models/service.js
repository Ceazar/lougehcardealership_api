'use strict';
module.exports = (sequelize, DataTypes) => {
  const Service = sequelize.define('Service', {
    // id: {type: DataTypes.NUMBER, primaryKey: true},
    servicename: DataTypes.STRING,
    description: DataTypes.STRING,
    pricerate: DataTypes.FLOAT,
    image: DataTypes.STRING,
    duration: DataTypes.NUMBER,
    mechanics: DataTypes.NUMBER
  }, {});
  Service.associate = function(models) {
    // associations can be defined here
    
  };
  return Service;
};