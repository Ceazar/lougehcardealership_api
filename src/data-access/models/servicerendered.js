'use strict';
module.exports = (sequelize, DataTypes) => {
  const servicerendered = sequelize.define('servicerendered', {
    servicenumber: DataTypes.STRING,
    serviceid: DataTypes.INTEGER,
    employeeid: DataTypes.STRING
  }, {});
  servicerendered.associate = function(models) {
    // associations can be defined here

    // servicerendered.belongsTo(models.Serviceticket, {
    //   foreignKey: 'servicenumber',
    //   constraints: false
    // })
  };
  return servicerendered;
};