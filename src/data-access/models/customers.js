'use strict';
module.exports = (sequelize, DataTypes) => {
  const Customers = sequelize.define('Customers', {
    // id: { type: DataTypes.NUMBER, primaryKey: true },
    customernumber: DataTypes.STRING,
    fname: DataTypes.STRING,
    mi: DataTypes.STRING,
    lname: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING
  }, {});
  Customers.associate = function (models) {
    // associations can be defined here

  };
  return Customers;
};