const { Cartobuy, Cars } = require('../../models');
const { Salesperson } = require('../../models')

const cartobuyDb = () => {
    return Object.freeze({
        getCartobuy,
        findCartobuy,
        findCartobuyCustomer,
        addCartobuy,
        deleteCartobuy,
        deleteCartobuy2,
        updateCartobuy,
        findSchedule,
        countSalesperson,
        findCar,
        changeAvail
    })
}

async function changeAvail(id, available) {
    return Cars
        .update({
            available
        }, {
            where: {
                id
            }
        })
}

async function countSalesperson() {
    return Salesperson
        .count({ where: { status: 'AVAILABLE' } })
}

async function findSchedule(dateofreq) {
    return Cartobuy
        .findAll({ where: { dateofreq } })
}

async function updateCartobuy(cartobuyInfo) {
    // console.log('data access:',cartobuyInfo);
    return Cartobuy
        .update({
            carid: cartobuyInfo.carid,
            customernumber: cartobuyInfo.customernumber,
            dateofreq: cartobuyInfo.dateofreq,
            schedule: cartobuyInfo.schedule
        }, {
            where: { id: cartobuyInfo.id }
        })
}

async function getCartobuy() {
    return Cartobuy.findAll()
}

async function findCartobuy(id) {
    return Cartobuy
        .findAll({
            where: { id }
        })
}

async function findCartobuyCustomer(customernumber) {
    return Cartobuy
        .findAll({
            where: { customernumber }
        })
}

async function findCar(carid) {
    return Cartobuy
        .findAll({
            where: { carid }
        })
}
async function addCartobuy(carid, customernumber, dateofreq, schedule) {
    return Cartobuy
        .create({
            carid, customernumber, dateofreq, schedule
        })
}

async function deleteCartobuy(id) {
    return Cartobuy
        .destroy({
            where: { id }
        })
}

async function deleteCartobuy2(carid, customernumber) {
    return Cartobuy
        .destroy({
            where: { carid, customernumber }
        })
}

module.exports = cartobuyDb;