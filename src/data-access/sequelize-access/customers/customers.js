const { Customers } = require('../../models');

const customerDb = () => {
    return Object.freeze({
        getCustomers,
        findCustomer,
        createCustomer,
        updateCustomer,
        findCustomerNumber
    });
}

async function getCustomers() {
    return Customers.findAll()
}

async function findCustomer(id) {
    return Customers
        .findAll({
            where: { id }
        })
}

async function findCustomerNumber(customernumber) {
    return Customers
        .findAll({
            where: { customernumber }
        })
}

async function createCustomer(customernumber, fname, mi, lname, email, phone) {
    return Customers
        .create({
            customernumber, fname, mi, lname, email, phone
        })
}

async function updateCustomer(customerInfo) {
    return Customers
        .update({
            fname: customerInfo.fname,
            mi: customerInfo.mi,
            lname: customerInfo.lname,
            email: customerInfo.email,
            phone: customerInfo.email
        }, {
            where: {
                id: customerInfo.id
            }
        })
}

module.exports = customerDb;