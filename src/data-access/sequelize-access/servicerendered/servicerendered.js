const { servicerendered, Employees } = require("../../models");

const serviceDb = () => {
    return Object.freeze({
        findServicerendered,
        findAssigned
    });
};

async function findServicerendered(servicenumber) {
    return servicerendered
        .findAll({ where: { servicenumber } });
}

async function findAssigned(employeeid) {
    return servicerendered
        .findAll({ where: { employeeid } });
}

module.exports = serviceDb;
