const { Parts } = require('../../models');

const partsDb = () => {
    return Object.freeze({
        getParts,
        createPart,
        updatePart,
        findPart,
        deletePart
    })
}

async function deletePart(id) {
    return Parts
        .destroy({
            where: { id }
        })
}

async function findPart(id) {
    return Parts
        .findAll({
            where: { id }
        })
}

async function updatePart(partInfo) {
    return Parts
        .update({
            partname: partInfo.partname,
            description: partInfo.description,
            stock: partInfo.stock,
            price: partInfo.price
        }, {
            where: { id: partInfo.id }
        })
}

async function createPart(partname, description, stock, price) {
    return Parts
        .create({
            partname,
            description,
            stock,
            price
        })
}

async function getParts() {
    return Parts
        .findAll();
}

module.exports = partsDb;