const { Accounts } = require('../../models');
const { Employees } = require('../../models');

const accountDb = () => {
    return Object.freeze({
        getAccounts,
        findAccount,
        createAccount,
        updateAccount,
        deleteAccount,
        loginAccount,
        findbyEmployeeID
    });
}

async function findbyEmployeeID(userid) {
    return Accounts.findAll({ where: { userid } })
}

async function getAccounts() {
    return Accounts.findAll({
        attributes: ['username', 'role']
    })
}

async function findAccount(userid) {
    return Accounts
        .findAll({
            where: { userid }
        })
}

async function createAccount(userid, username, password, role) {
    return Accounts
        .create({
            userid,
            username,
            password,
            role
        })
}

async function updateAccount(accountInfo) {
    return Accounts
        .update({
            username: accountInfo.username,
            password: accountInfo.password,
            role: accountInfo.role
        }, {
            where: {
                userid: accountInfo.userid
            }
        })
}

async function deleteAccount(id) {
    return Accounts
        .destroy({
            where: { id }
        })
}

async function loginAccount(username, password) {
    return Accounts
        .findAll({
            where: {
                username,
                password
            },
            attributes: ['userid', 'username', 'role']
            // include: [{
            //     model: Employees,
            //     where: {
            //         employeeid: accountInfo.employeeid
            //     }
            // }]

        })
}


module.exports = accountDb;