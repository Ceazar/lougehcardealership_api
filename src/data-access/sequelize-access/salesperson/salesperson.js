const { Salesperson } = require("../../models");
var Sequelize = require("sequelize");
const Op = Sequelize.Op;

const mechananicDb = () => {
    return Object.freeze({
        viewSalesperson,
        findSalesperson,
        createSalesperson,
        updateSalesperson,
        deleteSalesperson,
        updatePriority,
        orderbyPriority,
        countSalesperson
    });
};

async function countSalesperson() {
    return Salesperson.count();
}

async function updatePriority(priority) {
    return Salesperson.update(
        {
            priority: Sequelize.literal("priority - 1")
        },
        {
            where: {
                priority: {
                    [Op.gt]: priority
                }
            }
        }
    );
}

async function deleteSalesperson(id) {
    return Salesperson.destroy({ where: { id } });
}

async function updateSalesperson(salespersonInfo) {
    return Salesperson.update(
        {
            employeeid: salespersonInfo.employeeid,
            priority: salespersonInfo.priority
        },
        {
            where: { id: salespersonInfo.id }
        }
    );
}

async function createSalesperson(employeeid, status, priority) {
    return Salesperson.create({
        employeeid,
        status,
        priority
    });
}

async function findSalesperson(id) {
    return Salesperson.findAll({
        where: { id }
    });
}

async function viewSalesperson() {
    return Salesperson.findAll();
}

async function orderbyPriority() {
    return Salesperson.findAll({
        attributes: ["priority"],
        order: [["priority", "DESC"]],
        limit: 1
    });
}

module.exports = mechananicDb;
