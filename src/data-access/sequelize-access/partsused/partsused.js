const { Partsused } = require("../../models");
const { Parts } = require("../../models");

const partsusedDb = () => {
    return Object.freeze({
        findPartsused,
        createPartsused,
        deletePartsused,
        updatePartsused,
        findPartsusedByid,
        updatePart,
        getServicedata,
        updatePartquantity,
        getPartused
    });
};

async function getPartused() {
    return Partsused.findAll({});
}

async function updatePart(id, quantity) {
    const oldQuantity = await Parts.findAll({
        attributes: ["stock"],
        where: { id }
    });
    const newQuantity = oldQuantity[0].stock - quantity;
    return Parts.update(
        {
            stock: newQuantity
        },
        {
            where: { id }
        }
    );
}

async function updatePartquantity(id, quantity, method) {
    var newQuantity = 0;
    const oldQuantity = await Parts.findAll({
        attributes: ["stock"],
        where: { id }
    });

    if (method == "ADD") {
        newQuantity = oldQuantity[0].stock + quantity;
    } else if (method == "SUBTRACT") {
        newQuantity = oldQuantity[0].stock - quantity;
    }
    return Parts.update(
        {
            stock: newQuantity
        },
        {
            where: { id }
        }
    );
}

async function getServicedata(id) {
    return Parts.findAll({ where: { id } });
}

async function findPartsused(servicenumber) {
    return Partsused.findAll({ where: { servicenumber } });
}

async function findPartsusedByid(id) {
    return Partsused.findAll({ where: { id } });
}

async function createPartsused(partid, servicenumber, quantity) {
    return Partsused.create({
        partid,
        servicenumber,
        quantity
    });
}

async function deletePartsused(id) {
    return Partsused.destroy({ where: { id } });
}

async function updatePartsused(partsusedInfo) {
    return Partsused.update(
        {
            partid: partsusedInfo.partid,
            servicenumber: partsusedInfo.servicenumber,
            quantity: partsusedInfo.quantity
        },
        {
            where: {
                id: partsusedInfo.id
            }
        }
    );
}

module.exports = partsusedDb;
