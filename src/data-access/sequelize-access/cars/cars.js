const { Cars } = require('../../models');

const carDb = () => {
    return Object.freeze({
        getCars,
        findCar,
        addCar,
        updateCar,
        uploadCar,
        findCarbyid,
        getCarstock,
        findVin,
        getCarColors
    })
}

async function getCarstock(model) {
    return Cars
        .count({
            where: { model }
        })
}

async function uploadCar(carDetails) {
    return Cars
        .update({
            carimage: carDetails.carimage
        }, {
            where: { id: carDetails.id }
        })
}

async function getCars() {
    return Cars.findAll({
        attributes: [
            'id',
            'vin',
            'model',
            'company',
            'color',
            'status',
            'price',
            'available',
            'carimage',
            'horsepower',
            'transmission',
            'enginedisplacement',
            'fueltype'
        ]
    }, { where: { available: "TRUE" } })
}

async function getCarColors(carDetails) {
    return Cars.findAll({
        where: {
            model: carDetails.model,
            company: carDetails.company,
            status: carDetails.status,
            price: carDetails.price
        }
    })
}

async function findCar(id) {
    return Cars
        .findAll({
            where: { id },
            attributes: [
                'id',
                'vin',
                'model',
                'company',
                'color',
                'status',
                'price',
                'available',
                'carimage',
                'horsepower',
                'transmission',
                'enginedisplacement',
                'fueltype'
            ]
        })
}

async function findVin(vin) {
    return Cars
        .findAll({
            where: { vin },
            attributes: [
                'vin'
            ]
        })
}


async function findCarbyid(id) {
    return Cars
        .findAll({
            where: { id }
        })
}

async function addCar(vin, model, company, color, status, price, available, horsepower, transmission, enginedisplacement, fueltype) {
    return Cars
        .create({
            vin,
            model,
            company,
            color,
            status,
            price,
            available,
            horsepower,
            transmission,
            enginedisplacement,
            fueltype
        })
}

async function updateCar(carInfo) {
    return Cars
        .update({
            vin: carInfo.vin,
            model: carInfo.model,
            company: carInfo.company,
            color: carInfo.color,
            status: carInfo.status,
            price: carInfo.price,
            available: carInfo.available,
            horsepower: carInfo.horsepower,
            transmission: carInfo.transmission,
            enginedisplacement: carInfo.enginedisplacement,
            fueltype: carInfo.fueltype
        },
            {
                where: {
                    id: carInfo.id
                }
            })
}

module.exports = carDb;