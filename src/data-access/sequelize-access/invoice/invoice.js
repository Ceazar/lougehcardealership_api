const { Invoice } = require('../../models');
// const { Cars } = require('../../models');

const invoiceDb = () => {
    return Object.freeze({
        getInvoice,
        findInvoice,
        createInvoice,
        updateInvoice,
        carSold,
        carInvoice
    })
};

async function carSold(carid) {
    return Invoice
        .findAll({
            where: { carid }
        })
}

async function carInvoice(carid, customernumber) {
    return Invoice
        .findAll({
            where: { carid, customernumber }
        })
}

async function updateInvoice(Invoiceinfo) {
    return Invoice
        .update({
            customernumber: Invoiceinfo.customernumber,
            employeeid: Invoiceinfo.employeeid,
            carid: Invoiceinfo.carid,
            serviceticketid: Invoiceinfo.serviceticketid,
            date: Invoiceinfo.date,
            type: Invoiceinfo.type,
            totalamount: Invoiceinfo.totalamount
        }, {
            where: { id: Invoiceinfo.id }
        })
}

async function createInvoice(customernumber, carid, serviceticketnumber, date, type, totalamount) {
    return Invoice
        .create({
            customernumber,
            carid,
            serviceticketnumber,
            date,
            type,
            totalamount
        })
}

async function findInvoice(id) {
    return Invoice
        .findAll({
            where: { id }
        })
}

async function getInvoice() {
    return Invoice.findAll()
}

module.exports = invoiceDb;