const { Service } = require("../../models");

const serviceDb = () => {
    return Object.freeze({
        getServices,
        findService,
        createService,
        updateService,
        deleteService,
        uploadService,
        getServicebyname
    });
};

async function uploadService(serviceInfo) {
    console.log(serviceInfo);
    return Service.update(
        {
            image: serviceInfo.image
        },
        {
            where: { id: serviceInfo.id }
        }
    );
}

async function getServices() {
    return Service.findAll();
}

async function getServicebyname(servicename) {
    return Service.findAll({ where: { servicename } });
}

async function findService(id) {
    return Service.findAll({
        where: { id }
    });
}

async function createService(
    servicename,
    description,
    pricerate,
    duration,
    mechanics
) {
    return Service.create({
        servicename,
        description,
        pricerate,
        duration,
        mechanics
    });
}

async function updateService(serviceInfo) {
    return Service.update(
        {
            servicename: serviceInfo.servicename,
            description: serviceInfo.description,
            pricerate: serviceInfo.pricerate,
            duration: serviceInfo.duration,
            mechanics: serviceInfo.mechanics
        },
        {
            where: { id: serviceInfo.id }
        }
    );
}

async function deleteService(id) {
    return Service.destroy({ where: { id } });
}

module.exports = serviceDb;
