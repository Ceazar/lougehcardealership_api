const { Mechanics } = require("../../models");

const mechananicDb = () => {
    return Object.freeze({
        viewMechanic,
        findMechanic,
        createMechanic,
        updateMechanic,
        deleteMechanic,
        availableMechanics,
        findMechanicbyEmpid,
        findLastPriority
    });
};

async function availableMechanics() {
    return Mechanics.findAll({ where: { status: "AVAILABLE" } });
}

async function deleteMechanic(id) {
    return Mechanics.destroy({ where: { id } });
}

async function updateMechanic(mechanicInfo) {
    return Mechanics.update(
        {
            employeeid: mechanicInfo.employeeid,
            serviceticketid: mechanicInfo.serviceticketid,
            status: mechanicInfo.status,
            priority: mechanicInfo.priority
        },
        {
            where: { id: mechanicInfo.id }
        }
    );
}

async function createMechanic(employeeid, status, priority) {
    return Mechanics.create({
        employeeid,
        status,
        priority
    });
}

async function findMechanic(id) {
    return Mechanics.findAll({
        where: { id }
    });
}

async function findLastPriority() {
    return Mechanics.findAll({
        attributes: ["priority"],
        order: [["priority", "DESC"]],
        limit: 1
    });
}

async function findMechanicbyEmpid(employeeid) {
    return Mechanics.findAll({
        where: { employeeid }
    });
}

async function viewMechanic() {
    return Mechanics.findAll();
}

module.exports = mechananicDb;
