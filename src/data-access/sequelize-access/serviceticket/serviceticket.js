const { Serviceticket } = require('../../models');
const { Mechanics } = require('../../models');
const { Service } = require('../../models');
const { servicerendered } = require('../../models')

const serviceticketdb = () => {
    return Object.freeze({
        createServiceticket,
        findServiceticket,
        getServiceticket,
        updateServiceticket,
        CancelSched,
        availableMechanics,
        serviceDuration,
        getAvailabletime,
        createRendered,
        mechanicbyPriority,
        updatePriority,
        getAllmechanics,
        getSchedules,
        checkSched,
        findServicenumber,
        findEmployeeswithSched,
        getPrioritynumber,
        updateFee,
        findServiceticketbyEmployee
    })
}

async function updateFee(servicenumber, oldFee, fee, method) {
    let newFee;

    if (method == 'ADD') {
        newFee = oldFee + fee;
        Serviceticket
            .update({
                servicefee: newFee
            }, { where: { servicenumber } })
    }
    else {
        newFee = oldFee - fee;
        Serviceticket
            .update({
                servicefee: newFee
            }, { where: { servicenumber } })
    }
}

async function getPrioritynumber(employeeid) {
    return Mechanics.findAll({ attributes: ['priority'], where: { employeeid } })
}

async function findEmployeeswithSched(servicenumber) {
    return servicerendered
        .findAll({ attributes: ['employeeid'], where: { servicenumber } })
}

async function checkSched(employeeid, servicenumber) {
    return servicerendered
        .findAll({
            attributes: ['id'],
            where: { employeeid, servicenumber }
        })
}

async function getSchedules(dateofappt) {
    return Serviceticket
        .findAll({
            attributes: ['servicenumber', 'starttime', 'endtime'],
            where: { dateofappt },
            order: ['servicenumber']
        })

}

async function getAllmechanics() {
    return Mechanics
        .findAll({
            where: { status: 'AVAILABLE' }
        })
}

async function updatePriority(employeeid, priority) {
    return Mechanics
        .update({
            priority
        },
            {
                where: { employeeid }
            })
}

async function mechanicbyPriority() {
    return Mechanics
        .findAll({
            attributes: ['employeeid', 'priority'],
            order: ['priority']
        })
}

async function createRendered(serviceid, servicenumber, employeeid) {
    return servicerendered
        .create({
            serviceid, servicenumber, employeeid
        })

}

async function getAvailabletime(dateofappt) {
    return Serviceticket
        .findAll({
            where: { dateofappt },
            order: [
                ['servicenumber', 'DESC']
            ],
            limit: 1
        })
}

async function serviceDuration(id) {
    return Service
        .findAll({
            where: { id }
        })
}

async function availableMechanics() {
    return Mechanics
        .count({ where: { status: 'AVAILABLE' } })
}


async function getServiceticket() {
    return Serviceticket
        .findAll();
}

async function createServiceticket(servicenumber, customernumber, dateofappt, starttime, endtime, status, servicefee) {
    return Serviceticket
        .create({
            servicenumber,
            customernumber,
            dateofappt,
            starttime,
            endtime,
            status,
            servicefee
        })
}

async function findServiceticket(id) {
    return Serviceticket
        .findAll({
            attributes: ['id', 'servicenumber', 'customernumber', 'dateofappt', 'starttime', 'endtime', 'status', 'servicefee'],
            where: { id }
        })
}

async function findServiceticketbyEmployee(customernumber) {
    return Serviceticket
        .findAll({
            attributes: ['id', 'servicenumber', 'customernumber', 'dateofappt', 'starttime', 'endtime', 'status', 'servicefee'],
            where: { customernumber }
        })
}

async function findServicenumber(servicenumber) {
    return Serviceticket
        .findAll({
            attributes: ['id', 'servicenumber', 'customernumber', 'dateofappt', 'starttime', 'endtime', 'status', 'servicefee'],
            where: { servicenumber }
        })
}

async function updateServiceticket(serviceticketinfo) {
    return Serviceticket
        .update({
            customernumber: serviceticketinfo.customernumber,
            employeeid: serviceticketinfo.employeeid,
            serviceid: serviceticketinfo.serviceid,
            dateofappt: serviceticketinfo.dateofappt,
            servicefee: serviceticketinfo.servicefee,
            status: serviceticketinfo.status
        }, {
            where: { id: serviceticketinfo.id }
        })
}

async function CancelSched(servicenumber) {
    await Serviceticket
        .destroy({
            where: { servicenumber }
        });
    await servicerendered
        .destroy({
            where: { servicenumber }
        })
}

module.exports = serviceticketdb;