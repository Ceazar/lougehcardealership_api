const Employees = require('../../models').Employees;

const employeeDb = () => {
    return Object.freeze({
        createemployee,
        getEmployees,
        findEmployee,
        updateEmployee,
        createEmpid,
        deleteEmployee
    });

    async function deleteEmployee(employeeid) {
        return Employees
            .destroy({
                where: { employeeid }
            })
    }

    async function createEmpid() {
        return Employees
            .findAll({
                order: [
                    ['id', 'DESC']
                ],
                attributes: ['id'],
                limit: 1
            })
    }

    async function getEmployees() {
        return Employees
            .findAll()
    }

    async function findEmployee(employeeid) {
        return Employees
            .findAll({
                where: { employeeid }
            })
    }

    async function createemployee(employeeid, fname, mi, lname, position, age, phone, email) {
        return Employees
            .create({
                employeeid,
                fname,
                mi,
                lname,
                position,
                age,
                phone,
                email
            })
    }

    async function updateEmployee(empInfo) {
        return Employees
            .update({
                fname: empInfo.fname,
                mi: empInfo.mi,
                lname: empInfo.lname,
                position: empInfo.position,
                age: empInfo.age,
                phone: empInfo.phone,
                email: empInfo.email
            },
                {
                    where: {
                        employeeid: empInfo.employeeid
                    }
                });
    }

}


module.exports = employeeDb;