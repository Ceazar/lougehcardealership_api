'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Servicetickets', [{
      id: 1,
      servicenumber: "11111",
      dateofappt: "2019-10-10",
      starttime: "8:00 AM",
      endtime: "10:00 AM",
      status: "PENDING",
      serviceid: 1,
      customernumber: "15415745187",
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Servicetickets', null, {});

  }
};