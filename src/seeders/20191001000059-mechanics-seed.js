'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Mechanics', [{
      id: 1,
      employeeid: "15415152",
      status: "AVAILABLE",
      priority: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Mechanics', null, {});

  }
};