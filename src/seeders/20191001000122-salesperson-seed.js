'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Salespeople', [{
      id: 1,
      employeeid: "15415151",
      status: "AVAILABLE",
      priority: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Salespeople', null, {});

  }
};