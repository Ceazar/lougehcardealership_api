'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Cartobuys', [{
      id: 1,
      carid: 1,
      customernumber: "123456",
      dateofreq: "2019-10-2",
      schedule: "8:00 AM - 10:00 AM",
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Cartobuys', null, {});

  }
};