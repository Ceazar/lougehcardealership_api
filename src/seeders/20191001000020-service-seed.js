'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Services', [{
      servicename: "Dummy service name",
      description: "Dummy description",
      duration: 30,
      mechanics: 1,
      pricerate: 1000,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Services', null, {});

  }
};