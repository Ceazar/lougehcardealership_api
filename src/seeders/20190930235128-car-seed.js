'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Cars', [{
      id: 1,
      vin: "VIN001",
      model: "CarModeltest",
      company: "Companytest",
      color: "Black",
      status: "BRAND NEW",
      price: 5000000,
      available: "TRUE",
      carimage: "",
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {}),
      queryInterface.bulkInsert('Cars', [{
        id: 2,
        vin: "VIN0044",
        model: "CarModeltest",
        company: "Companytest",
        color: "Black",
        status: "BRAND NEW",
        price: 5000000,
        available: "TRUE",
        carimage: "",
        createdAt: new Date(),
        updatedAt: new Date(),
      }], {}),
      queryInterface.bulkInsert('Cars', [{
        id: 3,
        vin: "VIN00411",
        model: "CarModeltest",
        company: "Companytest",
        color: "Black",
        status: "BRAND NEW",
        price: 5000000,
        available: "TRUE",
        carimage: "",
        createdAt: new Date(),
        updatedAt: new Date(),
      }], {})


  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Cars', null, {});
  }
};
