'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Partsused', [{
      id: 1,
      servicenumber: "11111",
      partid: 1,
      quantity: 10,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Partsused', null, {});

  }
};