'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Invoices', [{
      id: 1,
      serviceticketnumber: "11111",
      date: new Date(),
      type: "Sales Invoice",
      totalamount: 1000000,
      customernumber: "123456",
      carid: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Invoices', null, {});

  }
};