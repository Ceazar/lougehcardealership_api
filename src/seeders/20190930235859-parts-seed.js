'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Parts', [{
      id: 1,
      partname: "Dummy partname",
      description: "Dummy description",
      stock: 5000,
      price: 500,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Parts', null, {});

  }
};