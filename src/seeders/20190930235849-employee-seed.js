'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.bulkInsert('Employees', [{
          id: 1,
          employeeid: "15415150",
          fname: "Employee",
          mi: "D",
          lname: "Dummy",
          position: "Admin",
          age: 30,
          phone: "013141",
          email: "employeedummy@dummies.com",
          createdAt: new Date(),
          updatedAt: new Date(),
        }], {}),
        queryInterface.bulkInsert('Employees', [{
          id: 2,
          employeeid: "15415151",
          fname: "Employee",
          mi: "D",
          lname: "Dummy",
          position: "Salesperson",
          age: 30,
          phone: "013141",
          email: "employeedummy@dummies.com",
          createdAt: new Date(),
          updatedAt: new Date(),
        }], {}),
        queryInterface.bulkInsert('Employees', [{
          id: 3,
          employeeid: "15415152",
          fname: "Employee",
          mi: "D",
          lname: "Dummy",
          position: "Mechanic",
          age: 30,
          phone: "013141",
          email: "employeedummy@dummies.com",
          createdAt: new Date(),
          updatedAt: new Date(),
        }], {})
      ])
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Employees', null, {});

  }
};
