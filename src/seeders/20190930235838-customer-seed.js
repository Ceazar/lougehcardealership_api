'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Customers', [{
      id: 1,
      customernumber: "123456",
      fname: "Dummy",
      mi: "X",
      lname: "Data",
      phone: "013141",
      email: "dummyaccount@dummies.com",
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Customers', null, {});

  }
};
