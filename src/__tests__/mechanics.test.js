const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

let token;

beforeAll(done => {
    request
        .post("/api/accounts/login")
        .send({
            username: "admin",
            password: "admin"
        })
        .end((err, response) => {
            token = response.body.token;
            done();
        });
});

describe("Mechanics suites.", () => {
    test("should get lists of mechanics", async done => {
        const response = await request
            .get("/api/mechanic/list")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get lists of mechanics without Authentication", async done => {
        const response = await request.get("/api/mechanic/list");

        expect(response.status).toBe(403);
        done();
    });

    test("should get lists of available mechanics", async done => {
        const response = await request
            .get("/api/mechanic/available")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get lists of available mechanics without Authentication", async done => {
        const response = await request.get("/api/mechanic/available");

        expect(response.status).toBe(403);
        done();
    });

    // test("should create a dummy mechanic", async done => {
    //     const response = await request
    //         .post("/api/mechanic/create")
    //         .send({
    //             id: 2,
    //             employeeid: "15415151",
    //             priority: 2
    //         })
    //         .set("Authorization", token);

    //     expect(response.status).toBe(201);
    //     done();
    // });

    test("should NOT create a dummy mechanic without Authentication", async done => {
        const response = await request.post("/api/mechanic/create").send({
            employeeid: "11111115"
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT create a dummy mechanic if employeeid exist", async done => {
        const response = await request
            .post("/api/mechanic/create")
            .send({
                employeeid: "15415152"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should get information of mechanic", async done => {
        const response = await request
            .get("/api/mechanic/1")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get information of mechanic without Authentication", async done => {
        const response = await request.get("/api/mechanic/1");

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT get information of mechanic if mechanic does not exist", async done => {
        const response = await request
            .get("/api/mechanic/11234")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should update mechanic", async done => {
        const response = await request
            .put("/api/mechanic/2")
            .send({
                employeeid: "15415151"
            })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT update mechanic without Authentication", async done => {
        const response = await request.put("/api/mechanic/5").send({
            employeeid: "15415151"
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT update mechanic with empty fields.", async done => {
        const response = await request
            .put("/api/mechanic/1")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    // test("should delete dummy mechanic", async done => {
    //     const response = await request
    //         .delete("/api/mechanic/2")
    //         .set("Authorization", token);

    //     expect(response.status).toBe(200);
    //     done();
    // });

    test("should NOT delete dummy mechanic if mechanic does not exist", async done => {
        const response = await request
            .delete("/api/mechanic/2")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT delete dummy mechanic without Authentication", async done => {
        const response = await request.delete("/api/mechanic/2");

        expect(response.status).toBe(403);
        done();
    });
});
