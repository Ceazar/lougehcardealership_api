const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

let token;

beforeAll(done => {
    request
        .post("/api/accounts/login")
        .send({
            username: "admin",
            password: "admin"
        })
        .end((err, response) => {
            token = response.body.token;
            done();
        });
});

describe("Parts suites.", () => {
    test("should get lists of parts", async done => {
        const response = await request
            .get("/api/parts/list")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get lists of parts without Authentication", async done => {
        const response = await request.get("/api/parts/list");

        expect(response.status).toBe(403);
        done();
    });

    // test("should create a part", async done => {
    //     const response = await request
    //         .post("/api/parts/create")
    //         .send({
    //             id: 2,
    //             partname: "dummypart",
    //             description: "description dummy",
    //             stock: 100,
    //             price: 100
    //         })
    //         .set("Authorization", token);

    //     expect(response.status).toBe(201);
    //     done();
    // });

    test("should NOT create a part without Authentication", async done => {
        const response = await request.post("/api/parts/create").send({
            partname: "dummypart",
            description: "description dummy",
            stock: 100,
            price: 100
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT create a part with incomplete fields.", async done => {
        const response = await request
            .post("/api/parts/create")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should get information of part", async done => {
        const response = await request
            .get("/api/parts/1")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get information of part without Authentication", async done => {
        const response = await request.get("/api/parts/3");

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT get information of part with invalid part id", async done => {
        const response = await request
            .get("/api/parts/2211")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should update part", async done => {
        const response = await request
            .put("/api/parts/2")
            .send({
                partname: "dummypart",
                description: "description dummy",
                stock: 100,
                price: 100
            })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT update part without Authentication", async done => {
        const response = await request.put("/api/parts/3").send({
            partname: "dummypart",
            description: "description dummy",
            stock: 100,
            price: 100
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT update part with incomplete field", async done => {
        const response = await request
            .put("/api/parts/3")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    // test("should delete part", async done => {
    //     const response = await request
    //         .delete("/api/parts/1")
    //         .set("Authorization", token);

    //     expect(response.status).toBe(200);
    //     done();
    // });

    test("should NOT delete part with invalid part id", async done => {
        const response = await request
            .delete("/api/parts/21231")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT delete part without Authentication", async done => {
        const response = await request.delete("/api/parts/21231");

        expect(response.status).toBe(403);
        done();
    });
});
