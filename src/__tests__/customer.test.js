const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

let token;

beforeAll(done => {
    request
        .post("/api/accounts/login")
        .send({
            username: "admin",
            password: "admin"
        })
        .end((err, response) => {
            token = response.body.token;
            done();
        });
});

describe("Customer suites.", () => {
    test("should get lists of customers", async done => {
        const response = await request
            .get("/api/customers/list")
            .set("Authorization", token);

        expect(response.status).toBe(200);

        done();
    });

    test("should NOT get lists of customers without Authentication", async done => {
        const response = await request
            .get("/api/customers/list")

        expect(response.status).toBe(403);

        done();
    });

    // test("should create a customer", async done => {
    //     const response = await request
    //         .post("/api/customers/create")
    //         .send({
    //             id: 2,
    //             fname: "dummy",
    //             mi: "d",
    //             lname: "dummy",
    //             phone: "08935259",
    //             email: "dummy@gmail.com"
    //         })
    //         .set("Authorization", token);

    //     expect(response.status).toBe(201);
    //     done();
    // });

    test("should NOT create a customer without Authentication", async done => {
        const response = await request
            .post("/api/customers/create")
            .send({
                fname: "dummy",
                mi: "d",
                lname: "dummy",
                phone: "08935259",
                email: "dummy@gmail.com"
            })

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT create a customer when fields are empty.", async done => {
        const response = await request
            .post("/api/customers/create")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should get information of customer", async done => {
        const response = await request
            .get("/api/customers/1")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get information of customer without Authentication", async done => {
        const response = await request
            .get("/api/customers/1234")

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT get information of customer when customer does not exist.", async done => {
        const response = await request
            .get("/api/customers/15697257583251")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should update customer", async done => {
        const response = await request
            .put("/api/customers/2")
            .send({
                fname: "dummy",
                mi: "d",
                lname: "dummy",
                phone: "08935259",
                email: "dummy@gmail.com"
            })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT update customer without Authentication", async done => {
        const response = await request
            .put("/api/customers/2")
            .send({
                fname: "dummy",
                mi: "d",
                lname: "dummy",
                phone: "08935259",
                email: "dummy@gmail.com"
            })

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT update customer when fields are empty", async done => {
        const response = await request
            .put("/api/customers/1569725758325")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });
});
