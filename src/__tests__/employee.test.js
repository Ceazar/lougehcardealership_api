const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

let token;

beforeAll(done => {
    request
        .post("/api/accounts/login")
        .send({
            username: "admin",
            password: "admin"
        })
        .end((err, response) => {
            token = response.body.token;
            done();
        });
});

describe("Employees suites.", () => {
    test("should get lists of employees", async done => {
        const response = await request
            .get("/api/employees/list")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get lists of employees without Authentication.", async done => {
        const response = await request
            .get("/api/employees/list")

        expect(response.status).toBe(403);
        done();
    });

    test("should generate new employeeid", async done => {
        const response = await request
            .get("/api/employees/empid")
            .send({ prefix: "1541515" })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT generate new employeeid without Authentication", async done => {
        const response = await request
            .get("/api/employees/empid")
            .send({ prefix: "1541515" })

        expect(response.status).toBe(403);
        done();
    });


    test("should NOT generate new employeeid without prefix provided.", async done => {
        const response = await request
            .get("/api/employees/empid")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    // test("should create an employee", async done => {
    //     const response = await request
    //         .post("/api/employees/create")
    //         .send({
    //             id: 4,
    //             employeeid: "15415153",
    //             fname: "dummy",
    //             mi: "d",
    //             lname: "dummy",
    //             position: "Dummy",
    //             age: 50,
    //             phone: "08935259",
    //             email: "dummy@gmail.com"
    //         })
    //         .set("Authorization", token);

    //     expect(response.status).toBe(201);
    //     done();
    // });


    test("should NOT create an employee without Authentication", async done => {
        const response = await request
            .post("/api/employees/create")
            .send({
                employeeid: "11111115",
                fname: "dummy",
                mi: "d",
                lname: "dummy",
                position: "Dummy",
                age: 50,
                phone: "08935259",
                email: "dummy@gmail.com"
            })

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT create an employee if employeeid exist", async done => {
        const response = await request
            .post("/api/employees/create")
            .send({
                employeeid: "15415150",
                fname: "dummy",
                mi: "d",
                lname: "dummy",
                position: "Dummy",
                age: 50,
                phone: "08935259",
                email: "dummy@gmail.com"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT create an employee with incomplete fields.", async done => {
        const response = await request
            .post("/api/employees/create")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should get information of employee", async done => {
        const response = await request
            .get("/api/employees/15415150")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get information of employee without Authentication", async done => {
        const response = await request
            .get("/api/employees/11111114")

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT get information of employee if employeeid does not exist.", async done => {
        const response = await request
            .get("/api/employees/1151")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should update employee", async done => {
        const response = await request
            .put("/api/employees/15415151")
            .send({
                fname: "dummy",
                mi: "d",
                lname: "dummy",
                position: "Dummy",
                age: 50,
                phone: "08935259",
                email: "dummy@gmail.com"
            })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT update employee without Authentication", async done => {
        const response = await request
            .put("/api/employees/11111114")
            .send({
                fname: "dummy",
                mi: "d",
                lname: "dummy",
                position: "Dummy",
                age: 50,
                phone: "08935259",
                email: "dummy@gmail.com"
            })

        expect(response.status).toBe(403);
        done();
    });


    test("should NOT update employee with invalid employeeid", async done => {
        const response = await request
            .put("/api/employees/1141")
            .send({
                fname: "dummy",
                mi: "d",
                lname: "dummy",
                position: "Dummy",
                age: 50,
                phone: "08935259",
                email: "dummy@gmail.com"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT update employee with incomplete fields.", async done => {
        const response = await request
            .put("/api/employees/1141")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });
});
