const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

let token;

beforeAll(done => {
    request
        .post("/api/accounts/login")
        .send({
            username: "admin",
            password: "admin"
        })
        .end((err, response) => {
            token = response.body.token;
            done();
        });
});

describe("Services suites.", () => {
    test("should get lists of services", async done => {
        const response = await request
            .get("/api/services/list")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get lists of services without Authentication.", async done => {
        const response = await request.get("/api/services/list");

        expect(response.status).toBe(403);
        done();
    });

    // test("should create a dummy service", async done => {
    //     const response = await request
    //         .post("/api/services/create")
    //         .send({
    //             id: 2,
    //             servicename: "dummyservice4",
    //             description: "description dummy",
    //             duration: 100,
    //             mechanics: 1,
    //             pricerate: 5000
    //         })
    //         .set("Authorization", token);

    //     expect(response.status).toBe(201);
    //     done();
    // });

    test("should NOT create a dummy service without Authentication", async done => {
        const response = await request.post("/api/services/create").send({
            servicename: "dummyservice4",
            description: "description dummy",
            duration: 100,
            mechanics: 1,
            pricerate: 5000
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT create a dummy service with Empty fields.", async done => {
        const response = await request
            .post("/api/services/create")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT create a dummy service if service already exists.", async done => {
        const response = await request
            .post("/api/services/create")
            .send({
                servicename: "Dummy service name",
                description: "description dummy",
                duration: 100,
                mechanics: 1,
                pricerate: 5000
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should get information of service", async done => {
        const response = await request
            .get("/api/services/1")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get information of service without Authentication", async done => {
        const response = await request.get("/api/services/3");

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT get information of service with Invalid service ID.", async done => {
        const response = await request
            .get("/api/services/112314")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should update dummy service", async done => {
        const response = await request
            .put("/api/services/1")
            .send({
                servicename: "dummyservice",
                description: "description dummy",
                duration: 100,
                mechanics: 1,
                pricerate: 5000
            })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT update dummy service without authentication", async done => {
        const response = await request.put("/api/services/31312").send({
            servicename: "dummyservice",
            description: "description dummy",
            duration: 100,
            mechanics: 1,
            pricerate: 5000
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT update dummy service with Invalid service ID", async done => {
        const response = await request
            .put("/api/services/112314")
            .send({
                servicename: "dummyservice",
                description: "description dummy",
                duration: 100,
                mechanics: 1,
                pricerate: 5000
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT update dummy service with Empty fields", async done => {
        const response = await request
            .put("/api/services/1")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    const filePath = `${__dirname}/6358.jpg`;

    test("should upload service image", async done => {
        const response = await request
            .post("/api/services/upload/1")
            .attach("file", filePath)
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT upload service image without Authentication", async done => {
        const response = await request
            .post("/api/services/upload/1")
            .attach("file", filePath);

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT upload service image with missing file", async done => {
        const response = await request
            .post("/api/services/upload/1")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    // test("should delete dummy service", async done => {
    //     const response = await request
    //         .delete("/api/services/2")
    //         .set("Authorization", token);

    //     expect(response.status).toBe(200);
    //     done();
    // });

    test("should NOT delete dummy service without Authentication", async done => {
        const response = await request.delete("/api/services/3");

        expect(response.status).toBe(403);
        done();
    });
});
