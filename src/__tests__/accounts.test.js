const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

let token;

beforeAll(done => {
    request
        .post("/api/accounts/login")
        .send({
            username: "admin",
            password: "admin"
        })
        .end((err, response) => {
            token = response.body.token;
            done();
        });
});

// before(function truncateDatabase(done) {
//     var tableNames = _.pluck(_.values(sequelize.models), 'tableName');
//     sequelize.query('TRUNCATE TABLE ' + tableNames.join(', ')).asCallback(done);
// });


describe("Accounts suites.", () => {
    test("should login with admin account.", async done => {
        const response = await request
            .post("/api/accounts/login")
            .send({ username: "admin", password: "admin" });

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT login with incorrect credentials.", async done => {
        const response = await request
            .post("/api/accounts/login")
            .send({ username: "admin", password: "wrong" });

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT login with empty/incomplete credentials.", async done => {
        const response = await request.post("/api/accounts/login");

        expect(response.status).toBe(400);
        done();
    });

    test("should get lists of accounts.", async done => {
        const response = await request
            .get("/api/accounts/list")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get lists of accounts with no Authentication.", async done => {
        const response = await request
            .get("/api/accounts/list")

        expect(response.status).toBe(403);
        done();
    });

    test("should get account information.", async done => {
        const response = await request
            .get("/api/accounts/1")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should get account information with no Authentication.", async done => {
        const response = await request
            .get("/api/accounts/1")

        expect(response.status).toBe(403);
        done();
    });

    test("should return an error when inputing an invalid account id.", async done => {
        const response = await request
            .get("/api/accounts/invalid")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should create an account.", async done => {
        const response = await request
            .post("/api/accounts/create")
            .send({
                userid: "15415151",
                username: "dummy",
                password: "account",
                role: "Salesperson"
            })
            .set("Authorization", token);

        expect(response.status).toBe(201);
        done();
    });

    test("should return an error when userid already exists.", async done => {
        const response = await request
            .post("/api/accounts/create")
            .send({
                userid: "15415150",
                username: "dummy",
                password: "account",
                role: "dummy"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should update dummy account.", async done => {
        const response = await request
            .put("/api/accounts/2")
            .send({
                username: "dummyupdated",
                password: "accountupdated",
                role: "dummyupdated"
            })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT update dummy account without Authentication.", async done => {
        const response = await request
            .put("/api/accounts/2")
            .send({
                username: "dummyupdated",
                password: "accountupdated",
                role: "dummyupdated"
            })

        expect(response.status).toBe(403);
        done();
    });


    test("should NOT update acount when account id does not exist.", async done => {
        const response = await request
            .put("/api/accounts/5415100")
            .send({
                username: "dummyupdated",
                password: "accountupdated",
                role: "dummyupdated"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT update acount when userid is invalid.", async done => {
        const response = await request
            .put("/api/accounts/invalid")
            .send({
                username: "dummyupdated",
                password: "accountupdated",
                role: "dummyupdated"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT update acount when fields are incomplete.", async done => {
        const response = await request
            .put("/api/accounts/6")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should delete account.", async done => {
        const response = await request
            .delete("/api/accounts/2")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });


    test("should NOT delete account without Authentication.", async done => {
        const response = await request
            .delete("/api/accounts/2")

        expect(response.status).toBe(403);
        done();
    });

    test("should not delete account when userid does not exist.", async done => {
        const response = await request
            .delete("/api/accounts/12345")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });


    test("should not delete account when userid is invalid.", async done => {
        const response = await request
            .delete("/api/accounts/invalid")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });
});
