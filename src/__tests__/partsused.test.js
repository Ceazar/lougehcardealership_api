const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

let token;

beforeAll(done => {
    request
        .post("/api/accounts/login")
        .send({
            username: "admin",
            password: "admin"
        })
        .end((err, response) => {
            token = response.body.token;
            done();
        });
});

describe("Partsused suites.", () => {
    test("should get lists of partsuseds", async done => {
        const response = await request
            .get("/api/partsused/list")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get lists of partsuseds without Authentication", async done => {
        const response = await request.get("/api/partsused/list");

        expect(response.status).toBe(403);
        done();
    });

    // test("should create a dummy partsused", async done => {
    //     const response = await request
    //         .post("/api/partsused/create")
    //         .send({
    //             id: 2,
    //             servicenumber: "11111",
    //             partid: 1,
    //             quantity: 4
    //         })
    //         .set("Authorization", token);

    //     expect(response.status).toBe(201);
    //     done();
    // });

    test("should NOT create a dummy partsused without Authentication", async done => {
        const response = await request.post("/api/partsused/create").send({
            servicenumber: "1231",
            partid: 4,
            quantity: 4
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT create a dummy partsused with Incomplete fields", async done => {
        const response = await request
            .post("/api/partsused/create")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should get information of partused based on servicenumber", async done => {
        const response = await request
            .get("/api/partsused/11111")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get information of partused based on servicenumber without Authentication", async done => {
        const response = await request.get("/api/partsused/1569578906833");

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT get information of partused with invalid servicenumber", async done => {
        const response = await request
            .get("/api/partsused/1241")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should update partsused", async done => {
        const response = await request
            .put("/api/partsused/1")
            .send({
                servicenumber: "1569578641248",
                partid: 14,
                quantity: 5
            })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT update partsused without Authentication", async done => {
        const response = await request.put("/api/partsused/2").send({
            servicenumber: "1569578641248",
            partid: 3,
            quantity: 5
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT update partsused with incomplete fields.", async done => {
        const response = await request
            .put("/api/partsused/3")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    // test("should delete partsused", async done => {
    //     const response = await request
    //         .delete("/api/partsused/1")
    //         .set("Authorization", token);

    //     expect(response.status).toBe(200);
    //     done();
    // });

    test("should NOT delete partsused without Authentication", async done => {
        const response = await request.delete("/api/partsused/5");

        expect(response.status).toBe(403);
        done();
    });
});
