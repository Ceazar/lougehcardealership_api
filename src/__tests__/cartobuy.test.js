const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

let token;

beforeAll(done => {
    request
        .post("/api/accounts/login")
        .send({
            username: "admin",
            password: "admin"
        })
        .end((err, response) => {
            token = response.body.token;
            done();
        });
});

describe("Cartobuy suites.", () => {
    test("should get lists of cartobuy", async done => {
        const response = await request
            .get("/api/cartobuy/list")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get lists of cartobuy without Authentication.", async done => {
        const response = await request
            .get("/api/cartobuy/list")

        expect(response.status).toBe(403);
        done();
    });

    test("should get new schedule of cartobuy", async done => {
        const response = await request
            .get("/api/cartobuy/schedule")
            .send({ dateofreq: "2019-11-2" })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get new schedule of cartobuy without Authentication", async done => {
        const response = await request
            .get("/api/cartobuy/schedule")
            .send({ dateofreq: "2019-11-29" })

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT get new schedule when date is invalid", async done => {
        const response = await request
            .get("/api/cartobuy/schedule")
            .send({ dateofreq: "1999-09-29" })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    // test("should create a cartobuy", async done => {
    //     const response = await request
    //         .post("/api/cartobuy/create")
    //         .send({
    //             id: 2,
    //             carid: 2,
    //             customernumber: "123456",
    //             dateofreq: "2019-09-31",
    //             schedule: "10:00 AM - 12:00AM"
    //         })
    //         .set("Authorization", token);

    //     expect(response.status).toBe(201);
    //     done();
    // });

    test("should NOT create a cartobuy without Authentication", async done => {
        const response = await request
            .post("/api/cartobuy/create")
            .send({
                carid: 1,
                customernumber: "1234",
                dateofreq: "2019-09-31",
                schedule: "10:00 AM - 12:00AM"
            })

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT create a cartobuy when carid is already reserved.", async done => {
        const response = await request
            .post("/api/cartobuy/create")
            .send({
                carid: 1,
                customernumber: "123456",
                dateofreq: "2019-09-31",
                schedule: "10:00 AM - 12:00AM"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT create a cartobuy when fields are empty.", async done => {
        const response = await request
            .post("/api/cartobuy/create")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should get information of cartobuy", async done => {
        const response = await request
            .get("/api/cartobuy/1")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get information of cartobuy without Authentication", async done => {
        const response = await request
            .get("/api/cartobuy/2")

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT get information of cartobuy when carid does not exist.", async done => {
        const response = await request
            .get("/api/cartobuy/1000")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should update cartobuy", async done => {
        const response = await request
            .put("/api/cartobuy/1")
            .send({
                carid: 2,
                customernumber: "123456",
                dateofreq: "2019-09-29",
                schedule: "3:00 PM - 5:00 PM"
            })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should delete dummy cartobuy", async done => {
        const response = await request
            .delete("/api/cartobuy/1")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT delete dummy cartobuy with invalid id", async done => {
        const response = await request
            .delete("/api/cartobuy/111415123")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT delete dummy cartobuy without Authentication.", async done => {
        const response = await request
            .delete("/api/cartobuy/1")

        expect(response.status).toBe(403);
        done();
    });
});
