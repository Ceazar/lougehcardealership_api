const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

let token;

beforeAll(done => {
    request
        .post("/api/accounts/login")
        .send({
            username: "admin",
            password: "admin"
        })
        .end((err, response) => {
            token = response.body.token;
            done();
        });
});

describe("Serviceticket suites.", () => {
    test("should get lists of servicetickets", async done => {
        const response = await request
            .get("/api/serviceticket/list")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get lists of servicetickets without Authentication", async done => {
        const response = await request.get("/api/serviceticket/list");

        expect(response.status).toBe(403);
        done();
    });

    // test("should create a dummy serviceticket", async done => {
    //     const response = await request
    //         .post("/api/serviceticket/create")
    //         .send({
    //             id: 2141,
    //             dateofappt: "2019-11-10",
    //             servicenumber: "11112",
    //             serviceid: 1,
    //             customernumber: "1234"
    //         })
    //         .set("Authorization", token);

    //     expect(response.status).toBe(201);
    //     done();
    // });

    test("should NOT create a dummy serviceticket without Authentication", async done => {
        const response = await request.post("/api/serviceticket/create").send({
            dateofappt: "2019-09-30",
            serviceid: 2,
            customernumber: "1234"
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT create a dummy serviceticket with Invalid date.", async done => {
        const response = await request
            .post("/api/serviceticket/create")
            .send({
                dateofappt: "1990-09-29",
                serviceid: 1,
                customernumber: "1569657745947"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT create a dummy serviceticket with Incomplete fields", async done => {
        const response = await request
            .post("/api/serviceticket/create")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should get information of service ticket", async done => {
        const response = await request
            .get("/api/serviceticket/1")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get information of service ticket without Authentication", async done => {
        const response = await request.get("/api/serviceticket/1");

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT get information of service ticket with invalid serviceticketnumber", async done => {
        const response = await request
            .get("/api/serviceticket/241511")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should update service ticket", async done => {
        const response = await request
            .put("/api/serviceticket/1234")
            .send({
                customernumber: "1234",
                servicenumber: "1569804873801",
                status: "DONE",
                servicefee: 10000
            })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT update service ticket without Authentication", async done => {
        const response = await request.put("/api/serviceticket/25").send({
            servicenumber: "1569804873801",
            status: "DONE"
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT update service ticket with Incomplete fields.", async done => {
        const response = await request
            .put("/api/serviceticket/2")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT update service ticket with invalid ID.", async done => {
        const response = await request
            .put("/api/serviceticket/212314")
            .send({
                servicenumber: "",
                status: "DONE"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should delete service ticket", async done => {
        const response = await request
            .delete("/api/serviceticket/11111")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT delete service ticket without Authentication", async done => {
        const response = await request.delete("/api/serviceticket/4");

        expect(response.status).toBe(403);
        done();
    });
});
