const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

let token;

beforeAll(done => {
    request
        .post("/api/accounts/login")
        .send({
            username: "admin",
            password: "admin"
        })
        .end((err, response) => {
            token = response.body.token;
            done();
        });
});

describe("Invoice suites.", () => {
    test("should get lists of invoice", async done => {
        const response = await request
            .get("/api/invoice/list")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get lists of invoice without Authentication.", async done => {
        const response = await request.get("/api/invoice/list");

        expect(response.status).toBe(403);
        done();
    });

    // test("should create an invoice", async done => {
    //     const response = await request
    //         .post("/api/invoice/create")
    //         .send({
    //             id: 2,
    //             customernumber: "123456",
    //             carid: 2
    //         })
    //         .set("Authorization", token);

    //     expect(response.status).toBe(201);
    //     done();
    // });

    test("should NOT create an invoice without Authentication", async done => {
        const response = await request.post("/api/invoice/create").send({
            customernumber: "1234",
            carid: 1
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT create an invoice if car does not exist.", async done => {
        const response = await request
            .post("/api/invoice/create")
            .send({
                customernumber: "1234",
                carid: 1002
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT create an invoice if customer does not exist.", async done => {
        const response = await request
            .post("/api/invoice/create")
            .send({
                customernumber: "1234414",
                carid: 1002
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT create an invoice if car is already bought.", async done => {
        const response = await request
            .post("/api/invoice/create")
            .send({
                customernumber: "1234",
                carid: 1
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT create an invoice with incomplete fields.", async done => {
        const response = await request
            .post("/api/invoice/create")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should get information of invoice", async done => {
        const response = await request
            .get("/api/invoice/1")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get information of invoice without Authentication", async done => {
        const response = await request.get("/api/invoice/1");

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT get information of invoice with invalid id", async done => {
        const response = await request
            .get("/api/invoice/12341")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should update invoice", async done => {
        const response = await request
            .put("/api/invoice/2")
            .send({
                customernumber: "123456",
                carid: 3
            })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT update invoice without Authentication", async done => {
        const response = await request.put("/api/invoice/1").send({
            customernumber: "1234",
            carid: 10
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT update invoice when customer does not exist", async done => {
        const response = await request
            .put("/api/invoice/1")
            .send({
                customernumber: "123213414",
                carid: 10
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT update invoice with incomplete fields.", async done => {
        const response = await request
            .put("/api/invoice/1")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });
});
