const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

let token;

beforeAll(done => {
    request
        .post("/api/accounts/login")
        .send({
            username: "admin",
            password: "admin"
        })
        .end((err, response) => {
            token = response.body.token;
            done();
        });
});

describe("Car suites.", () => {
    test("should get lists of cars.", async done => {
        const response = await request
            .get("/api/cars/list")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });
    const vin = Date.now();

    test("should NOT get lists of cars with no Authentication.", async done => {
        const response = await request
            .get("/api/cars/list")

        expect(response.status).toBe(403);
        done();
    });

    // test("should insert car details.", async done => {
    //     const response = await request
    //         .post("/api/cars/create")
    //         .send({
    //             id: 2,
    //             vin: "VIN002",
    //             model: "2019 Dummy 5000",
    //             company: "Dummy",
    //             color: "Transparent",
    //             status: "BRAND NEW",
    //             price: 10000000,
    //             available: "false"
    //         })
    //         .set("Authorization", token);

    //     expect(response.status).toBe(201);
    //     done();
    // });

    test("should NOT insert car details with no Authentication.", async done => {
        const response = await request
            .post("/api/cars/create")
            .send({
                vin,
                model: "2019 Dummy 5000",
                company: "Dummy",
                color: "Transparent",
                status: "BRAND NEW",
                price: 10000000,
                available: "false"
            })

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT insert car details when VIN already exists.", async done => {
        const response = await request
            .post("/api/cars/create")
            .send({
                vin: "VIN001",
                model: "2019 Dummy 5000",
                company: "Dummy",
                color: "Transparent",
                status: "BRAND NEW",
                price: 10000000,
                available: "false"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT insert car details when fields are incomplete.", async done => {
        const response = await request
            .post("/api/cars/create")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT insert car details when price is invalid.", async done => {
        const response = await request
            .post("/api/cars/create")
            .send({
                vin: "DUM0025",
                model: "2019 Dummy 5000",
                company: "Dummy",
                color: "Transparent",
                status: "BRAND NEW",
                price: "aabdM",
                available: "false"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should update car", async done => {
        const response = await request
            .put("/api/cars/1")
            .send({
                vin: "VIN003",
                model: "2019 Dummy 5000s",
                company: "Dummy",
                color: "Transparent",
                status: "BRAND NEW",
                price: 10000001,
                available: "true"
            })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT update car when car id does not exist.", async done => {
        const response = await request
            .put("/api/cars/6012300")
            .send({
                vin: "DUM001b",
                model: "2019 Dummy 5000s",
                company: "Dummy",
                color: "Transparent",
                status: "BRAND NEW",
                price: 10000001,
                available: "true"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT update car with no Authentication.", async done => {
        const response = await request
            .put("/api/cars/6012300")
            .send({
                vin: "DUM001b",
                model: "2019 Dummy 5000s",
                company: "Dummy",
                color: "Transparent",
                status: "BRAND NEW",
                price: 10000001,
                available: "true"
            })

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT update car when car id is invalid.", async done => {
        const response = await request
            .put("/api/cars/invalid")
            .send({
                vin: "DUM001b",
                model: "2019 Dummy 5000s",
                company: "Dummy",
                color: "Transparent",
                status: "BRAND NEW",
                price: 10000001,
                available: "true"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should get car information.", async done => {
        const response = await request
            .get("/api/cars/1")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get car information when car id does not exist.", async done => {
        const response = await request
            .get("/api/cars/31241")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT get car information with no Authentication.", async done => {
        const response = await request
            .get("/api/cars/31241")

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT get car information when car id is invalid.", async done => {
        const response = await request
            .get("/api/cars/invalid")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    const filePath = `${__dirname}/6358.jpg`;

    test("should upload car image.", async done => {
        const response = await request
            .post("/api/cars/upload/1")
            .attach("file", filePath)
            .set("Authorization", token);

        console.log(response.text);
        expect(response.status).toBe(200);
        done();
    });

    test("should NOT upload car image when carid is invalid.", async done => {
        const response = await request
            .post("/api/cars/upload/134516")
            .attach("file", filePath)
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT upload car image with no Authentication.", async done => {
        const response = await request
            .post("/api/cars/upload/6")
            .attach("file", filePath)

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT upload car image when file is missing.", async done => {
        const response = await request
            .post("/api/cars/upload/6")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });
});
