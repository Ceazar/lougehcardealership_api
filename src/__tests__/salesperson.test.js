const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

let token;

beforeAll(done => {
    request
        .post("/api/accounts/login")
        .send({
            username: "admin",
            password: "admin"
        })
        .end((err, response) => {
            token = response.body.token;
            done();
        });
});

describe("Salesperson suites.", () => {
    test("should get lists of salesperson", async done => {
        const response = await request
            .get("/api/salesperson/list")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get lists of salesperson without Authentication", async done => {
        const response = await request.get("/api/salesperson/list");

        expect(response.status).toBe(403);
        done();
    });

    // test("should create a dummy salesperson", async done => {
    //     const response = await request
    //         .post("/api/salesperson/create")
    //         .send({
    //             id: 2,
    //             employeeid: "15415152"
    //         })
    //         .set("Authorization", token);

    //     expect(response.status).toBe(201);
    //     done();
    // });

    test("should NOT create a dummy salesperson without Authentication", async done => {
        const response = await request.post("/api/salesperson/create").send({
            employeeid: "154151522"
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT create a dummy salesperson with Empty fields.", async done => {
        const response = await request
            .post("/api/salesperson/create")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should get information of salesperson", async done => {
        const response = await request
            .get("/api/salesperson/1")
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT get information of salesperson without Authentication", async done => {
        const response = await request.get("/api/salesperson/2");

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT get information of salesperson with invalid salesperson ID", async done => {
        const response = await request
            .get("/api/salesperson/212314")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should update salesperson", async done => {
        const response = await request
            .put("/api/salesperson/1")
            .send({
                employeeid: "15415150"
            })
            .set("Authorization", token);

        expect(response.status).toBe(200);
        done();
    });

    test("should NOT update salesperson without Authentication", async done => {
        const response = await request.put("/api/salesperson/2").send({
            employeeid: "154151520"
        });

        expect(response.status).toBe(403);
        done();
    });

    test("should NOT update salesperson with invalid ID", async done => {
        const response = await request
            .put("/api/salesperson/2123")
            .send({
                employeeid: "154151520"
            })
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    test("should NOT update salesperson with Empty fields", async done => {
        const response = await request
            .put("/api/salesperson/2123")
            .set("Authorization", token);

        expect(response.status).toBe(400);
        done();
    });

    // test("should delete dummy salesperson", async done => {
    //     const response = await request
    //         .delete("/api/salesperson/2")
    //         .set("Authorization", token);

    //     expect(response.status).toBe(200);
    //     done();
    // });

    test("should NOT delete dummy salesperson without Authentication", async done => {
        const response = await request.delete("/api/salesperson/1");

        expect(response.status).toBe(403);
        done();
    });
});
