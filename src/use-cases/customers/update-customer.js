const { updateCustomer } = require('../../entities/customers/app');

const UC_updateCustomer = ({ customerDb }) => {
    return async function _updateCustomer(customerInfo) {
        const customerEntity = await updateCustomer(customerInfo);

        const data = {
            id: customerEntity.getId(),
            fname: customerEntity.getFname(),
            mi: customerEntity.getMi(),
            lname: customerEntity.getLname(),
            email: customerEntity.getEmail(),
            phone: customerEntity.getPhone()
        }

        await customerDb.updateCustomer({ ...data });

        return data;
    }  
}
module.exports = UC_updateCustomer;
