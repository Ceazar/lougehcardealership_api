const UC_getCustomers = ({ customerDb }) => {
    return async function getCustomers(){
        return customerDb.getCustomers();
    }
}
module.exports = UC_getCustomers;