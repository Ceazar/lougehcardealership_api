const { makeCustomer } = require('../../entities/customers/app');

const UC_createCustomer = ({ customerDb }, { accountDb }) => {
    return async function createCustomer(customerInfo) {
        const customerEntity = makeCustomer(customerInfo);

        const customer = await customerDb.createCustomer(
            customerEntity.getCustomernum(),
            customerEntity.getFname(),
            customerEntity.getMi(),
            customerEntity.getLname(),
            customerEntity.getEmail(),
            customerEntity.getPhone()
        )

        await accountDb.createAccount(
            customer.dataValues.customernumber,
            customerInfo.username,
            customerInfo.password,
            customerInfo.role
        )

        return customer;
    }
}

module.exports = UC_createCustomer;