const UC_findCustomer = ({ customerDb }) => {
    return async function findCustomer(id) {
        const decimal =  id.indexOf(".") == -1;
        if((!(Number.isInteger(Number(id)))) || id <= 0 || !decimal){
            throw new Error("Invalid Customer ID.");
        }
        const customer = await customerDb.findCustomer(id);
        if (!customer) {
            throw new Error("Customer does not exist.")
        }
        if (Object.entries(customer).length == 0) {
            throw new Error("Customer does not exist.");
        }
        return customer;
    }
}

module.exports = UC_findCustomer;