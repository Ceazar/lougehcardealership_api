const customerDb = require('../../data-access/sequelize-access/customers/app');
const accountDb = require('../../data-access/sequelize-access/accounts/app');

const UC_getCustomers = require('./view-customer');
const UC_createCustomer = require('./create-customer');
const UC_findCustomer = require('./find-customer');
const UC_updateCustomer = require('./update-customer');

const getCustomers = UC_getCustomers({ customerDb });
const createCustomer = UC_createCustomer({ customerDb }, { accountDb });
const findCustomer = UC_findCustomer({ customerDb });
const updateCustomer = UC_updateCustomer({ customerDb });

const customerService = Object.freeze({
    getCustomers,
    createCustomer,
    findCustomer,
    updateCustomer
})

module.exports = customerService;
module.exports = {
    getCustomers,
    createCustomer,
    findCustomer,
    updateCustomer
}