const { makeInvoice } = require('../../entities/invoice/app');

const UC_createInvoice = ({ invoiceDb, serviceticketDb, carDb, cartobuyDb }) => {
    return async function createInvoice(invoiceInfo) {
        const invoiceEntity = makeInvoice(invoiceInfo);
        var amount = 0;

        if (invoiceEntity.getCarid() != null) {
            const carInfo = await carDb.findCar(invoiceEntity.getCarid())
            if (carInfo.length == 0) {
                throw new Error("Car does not exist.")
            }
            amount = carInfo[0].price;
        }
        else {
            const ticketInfo = await serviceticketDb.findServicenumber(invoiceEntity.getServiceticketnumber());
            if (ticketInfo.length == 0) {
                throw new Error("Serviceticket does not exist.")
            }
            amount = ticketInfo[0].servicefee;
        }
        invoiceInfo.amount = amount;

        const check = await invoiceDb.carSold(invoiceEntity.getCarid());
        if (check.length != 0) {
            throw new Error("Car is already sold.");
        }

        await cartobuyDb.deleteCartobuy2(invoiceEntity.getCarid(), invoiceEntity.getCustomernumber())

        return invoiceDb.createInvoice(
            invoiceEntity.getCustomernumber(),
            invoiceEntity.getCarid(),
            invoiceEntity.getServiceticketnumber(),
            invoiceEntity.getDate(),
            invoiceEntity.getType(),
            amount
        );
    }
}

module.exports = UC_createInvoice;