const invoiceDb = require('../../data-access/sequelize-access/invoice/app');
const serviceticketDb = require('../../data-access/sequelize-access/serviceticket/app');
const carDb = require('../../data-access/sequelize-access/cars/app');
const cartobuyDb = require('../../data-access/sequelize-access/cartobuy/app');


const UC_createInvoice = require('./create-invoice');
const UC_findInvoice = require('./find-invoice');
const UC_updateInvoice = require('./update-invoice');
const UC_viewInvoice = require('./view-invoice');

const createInvoice = UC_createInvoice({ invoiceDb, serviceticketDb, carDb, cartobuyDb });
const findInvoice = UC_findInvoice({ invoiceDb });
const updateInvoice = UC_updateInvoice({ serviceticketDb, invoiceDb, carDb });
const getInvoice = UC_viewInvoice({ invoiceDb });

const invoiceService = Object.freeze({
    createInvoice,
    findInvoice,
    updateInvoice,
    getInvoice
});

module.exports = invoiceService,
    module.exports = {
        createInvoice,
        findInvoice,
        updateInvoice,
        getInvoice
    }