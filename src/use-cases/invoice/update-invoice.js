const { updateInvoice } = require('../../entities/invoice/app');

const UC_updateInvoice = ({ serviceticketDb, invoiceDb, carDb }) => {
    return async function _updateInvoice(invoiceInfo) {
        var amount = 0;

        if (invoiceInfo.carid) {
            const carInfo = await carDb.findCar(invoiceInfo.carid)
            if (carInfo.length == 0) {
                throw new Error("Car does not exist.")
            }
            amount = carInfo[0].price;
        }
        else if (invoiceInfo.servicenumber) {
            const ticketInfo = await serviceticketDb.findServicenumber(invoiceInfo.servicenumber);
            if (ticketInfo.length == 0) {
                throw new Error("Serviceticket does not exist.")
            }
            amount = ticketInfo[0].servicefee;
        }

        const check = await invoiceDb.carSold(invoiceInfo.carid);
        if (check.length != 0) {
            throw new Error("Car is already sold.");
        }

        invoiceInfo.amount = amount;
        const invoiceEntity = await updateInvoice(invoiceInfo);

        const data = {
            id: invoiceEntity.getId(),
            customernumber: invoiceEntity.getCustomernumber(),
            carid: invoiceEntity.getCarid(),
            serviceticketid: invoiceEntity.getServiceticketid(),
            date: invoiceEntity.getDate(),
            type: invoiceEntity.getType(),
            amount
        }

        await invoiceDb.updateInvoice({ ...data });

        return data;
    }
}

module.exports = UC_updateInvoice;