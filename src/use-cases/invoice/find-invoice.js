const UC_findInvoice = ({ invoiceDb }) => {
    return async function findInvoice(id) {
        const decimal = id.indexOf(".") == -1;
        if ((!(Number.isInteger(Number(id)))) || id <= 0 || !decimal) {
            throw new Error("Invalid Invoice ID.");
        }
        const invoice = await invoiceDb.findInvoice(id);
        if (!invoice || Object.entries(invoice).length == 0) {
            throw new Error("Invoice does not exist.")
        }
        return invoice;
    }
}

module.exports = UC_findInvoice;