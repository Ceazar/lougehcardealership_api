const UC_getInvoice = ({ invoiceDb }) => {
    return async function getInvoice() {
        return invoiceDb.getInvoice();
    }
}

module.exports = UC_getInvoice;