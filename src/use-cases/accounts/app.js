const accountDb = require('../../data-access/sequelize-access/accounts/app');

const UC_createAccount = require('./create-account');
const UC_getAccounts = require('./view-accounts');
const UC_findAccount = require('./find-account');
const UC_updateAccount = require('./update-account');
const UC_deleteAccount = require('./delete-account');
const UC_loginAccount = require('./login-account');

const createAccount = UC_createAccount({ accountDb });
const getAccounts = UC_getAccounts({ accountDb });
const findAccount = UC_findAccount({ accountDb });
const updateAccount = UC_updateAccount({ accountDb });
const deleteAccount = UC_deleteAccount({ accountDb });
const loginAccount = UC_loginAccount({ accountDb });

const accountService = Object.freeze({
    createAccount,
    getAccounts,
    findAccount,
    updateAccount,
    deleteAccount,
    loginAccount
});

module.exports = accountService;
module.exports = {
    createAccount,
    getAccounts,
    findAccount,
    updateAccount,
    deleteAccount,
    loginAccount
}