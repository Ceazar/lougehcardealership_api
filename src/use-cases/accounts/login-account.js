const { dologinAccount } = require('../../entities/accounts/app');

const UC_loginAccount = ({ accountDb }) => {
    return async function loginAccount(accountInfo) {
        const auth = dologinAccount(accountInfo);

        const login = await accountDb.loginAccount(auth.getUsername(), auth.getPassword());

        if (!login || Object.entries(login).length == 0) {
            throw new Error("Authentication failed.");
        }

        return login;
    }
}

module.exports = UC_loginAccount;