const { updateAccount } = require('../../entities/accounts/app');

const UC_updateAccount = ({ accountDb }) => {
    return async function _updateAccount(accountInfo) {
        const accountEntity = await updateAccount(accountInfo);

        console.log(accountInfo);

        // console.log(accountEntity.getUsername());
        const data = {
            userid: accountInfo.id,
            username: accountEntity.getUsername(),
            password: accountEntity.getPassword(),
            role: accountEntity.getRole()
        }

        console.log(data);

        // console.log(data);
        const checkAccount = await accountDb.findAccount(data.userid);

        if (!checkAccount || checkAccount.length == 0) {
            throw new Error("Account does not exist.")
        } else {
            await accountDb.updateAccount({ ...data });
        }


        return data;
    }
}

module.exports = UC_updateAccount;