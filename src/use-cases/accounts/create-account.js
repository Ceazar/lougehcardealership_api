const { makeAccount } = require('../../entities/accounts/app');

const UC_createAccount = ({ accountDb }) => {
    return async function createAccount(accountInfo) {
        const accountEntity = makeAccount(accountInfo);
        const checkAccount = await accountDb.findbyEmployeeID(accountEntity.getUserid());

        if (Object.entries(checkAccount).length != 0) {
            throw new Error("Account with Employee ID already exists.")
        }

        return accountDb.createAccount(
            accountEntity.getUserid(),
            accountEntity.getUsername(),
            accountEntity.getPassword(),
            accountEntity.getRole()
        )
    }
}

module.exports = UC_createAccount;