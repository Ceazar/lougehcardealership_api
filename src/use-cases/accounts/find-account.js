const UC_findAccount = ({ accountDb }) => {
    return async function findAccount(id) {
        const decimal =  id.indexOf(".") == -1;
        if((!(Number.isInteger(Number(id)))) || id <= 0 || !decimal){
            throw new Error("Invalid Account ID.");
        }
        const account = await accountDb.findAccount(id);
        
        if (!account) {
            throw new Error("Account does not exist.")
        }
        if (Object.entries(account).length == 0) {
            throw new Error("Account does not exist.");
        }

        return account;
    }
}
module.exports = UC_findAccount;