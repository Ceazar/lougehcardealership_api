const UC_deleteAccount = ({ accountDb }) => {
    return async function deleteAccount(id) {
        const account = await accountDb.findAccount(id);
        const deleteaccount = await accountDb.deleteAccount(id);

        // Check if account exist in database
        if (!account || Object.entries(account).length == 0) {
            throw new Error("Account does not exist.");
        }

        return deleteaccount, { Message: "Account has been deleted.", Data: account }
    }
}

module.exports = UC_deleteAccount;