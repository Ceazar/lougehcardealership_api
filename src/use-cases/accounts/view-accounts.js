const UC_getAccounts = ({ accountDb }) => {
    return async function getAccounts(){
        return accountDb.getAccounts();
    }
}
module.exports = UC_getAccounts;