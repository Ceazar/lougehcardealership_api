const { updateCartobuy } = require('../../entities/cartobuy/app');

const UC_updateCartobuy = ({ cartobuyDb }) => {
    return async function _updateCartobuy(cartobuyInfo) {
        console.log(cartobuyInfo);
        // console.log('use case:',cartobuyInfo);
        const cartobuyEntity = await updateCartobuy(cartobuyInfo);

        const data = {
            id: cartobuyInfo.id,
            carid: cartobuyEntity.getCarid(),
            customernumber: cartobuyEntity.getCustomernumber(),
            dateofreq: cartobuyEntity.getDateofreq(),
            schedule: cartobuyEntity.getSchedule()
        }

        await cartobuyDb.updateCartobuy({ ...data });

        return data;
    }
}

module.exports = UC_updateCartobuy