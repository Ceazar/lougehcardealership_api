const UC_findCartobuy = ({ cartobuyDb }, { carsDb }) => {
    return async function findCartobuy(id) {
        const cartobuy = await cartobuyDb.findCartobuyCustomer(id);

        for (let i = 0; i < cartobuy.length; i++) {
            const car = await carsDb.findCar(cartobuy[i].dataValues.carid)
            cartobuy[i].dataValues.carname = car[0].dataValues.model;
            cartobuy[i].dataValues.price = car[0].dataValues.price;
        }

        if (!cartobuy) {
            throw new Error("Data does not exist.");
        }
        if (Object.entries(cartobuy).length == 0) {
            throw new Error("Data does not exist.");
        }

        return cartobuy;

    }
}

module.exports = UC_findCartobuy;