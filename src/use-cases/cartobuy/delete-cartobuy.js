const UC_deleteCartobuy = ({ cartobuyDb }) => {
    return async function deleteCartobuy(id) {
        console.log(id);
        const cartobuy = await cartobuyDb.findCartobuy(id);
        const deletecartobuy = await cartobuyDb.deleteCartobuy(id);

        if (!cartobuy || Object.entries(cartobuy).length == 0) {
            throw new Error("Data does not exist.");
        }
        await cartobuyDb.changeAvail(cartobuy[0].carid, "TRUE");

        return deletecartobuy, { Message: "Successfully deleted data.", Data: cartobuy }
    }
}
module.exports = UC_deleteCartobuy;