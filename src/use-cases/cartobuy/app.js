const cartobuyDb = require('../../data-access/sequelize-access/cartobuy/app');
const carsDb = require('../../data-access/sequelize-access/cars/app');
const customerDb = require('../../data-access/sequelize-access/customers/app');
const invoiceDb = require('../../data-access/sequelize-access/invoice/app');

const UC_createCartobuy = require('./create-cartobuy');
const UC_updateCartobuy = require('./update-cartobuy');
const UC_deleteCartobuy = require('./delete-cartobuy');
const UC_getCartobuy = require('./view-cartobuy');
const UC_findCartobuy = require('./find-cartobuy');
const UC_findSchedule = require('./find-schedule');

const createCartobuy = UC_createCartobuy({ cartobuyDb });
const updateCartobuy = UC_updateCartobuy({ cartobuyDb });
const deleteCartobuy = UC_deleteCartobuy({ cartobuyDb });
const viewCartobuy = UC_getCartobuy({ cartobuyDb }, { carsDb }, { customerDb }, { invoiceDb });
const findCartobuy = UC_findCartobuy({ cartobuyDb }, { carsDb });
const findSchedule = UC_findSchedule({ cartobuyDb });

const cartobuyService = Object.freeze({
    createCartobuy,
    updateCartobuy,
    deleteCartobuy,
    viewCartobuy,
    findCartobuy,
    findSchedule
});

module.exports = cartobuyService;
module.exports = {
    createCartobuy,
    updateCartobuy,
    deleteCartobuy,
    viewCartobuy,
    findCartobuy,
    findSchedule
}