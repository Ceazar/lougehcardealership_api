const UC_getCartobuy = ({ cartobuyDb }, { carsDb }, { customerDb }, { invoiceDb }) => {
    return async function getCartobuy() {
        const cartobuy = await cartobuyDb.getCartobuy();

        for (let i = 0; i < cartobuy.length; i++) {
            const car = await carsDb.findCar(cartobuy[i].dataValues.carid);

            cartobuy[i].dataValues.carvin = car[0].dataValues.vin;
            cartobuy[i].dataValues.carname = car[0].dataValues.model;
            cartobuy[i].dataValues.carprice = car[0].dataValues.price;

            const customer = await customerDb.findCustomerNumber(cartobuy[i].dataValues.customernumber);

            cartobuy[i].dataValues.customername = customer[0].dataValues.lname + ", " + customer[0].dataValues.fname + " " + customer[0].dataValues.mi;

            const invoice = await invoiceDb.carInvoice(car[0].dataValues.id, customer[0].dataValues.customernumber)

            // if (invoice[0]) {
            //     cartobuy[i].dataValues.invoice = invoice[0].dataValues;
            // }

        }
        return cartobuy;
    }
}

module.exports = UC_getCartobuy;