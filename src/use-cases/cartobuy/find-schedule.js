const moment = require('moment');

const UC_findSchedule = ({ cartobuyDb }) => {
    return async function findSchedule(dateofreq) {
        const schedule = await cartobuyDb.findSchedule(dateofreq);
        const salespersons = await cartobuyDb.countSalesperson();

        // Check if date is not before today
        var as = moment(dateofreq, 'YYYY-MM-DD');
        var curtime = moment().format('YYYY-MM-DD');

        if (as.isBefore(curtime)) {
            throw new Error("Invalid date. Please input a date not before today.")
        }


        var schedules = ['8:00 AM - 10:00 AM', '10:00 AM - 12:00 PM', '1:00 PM - 3:00 PM', '3:00 PM - 5:00 PM'];

        var a = salespersons, b = salespersons, c = salespersons, d = salespersons;

        function removeSched(sched) {
            for (x = 0; x < schedules.length; x++) {
                if (schedules[x] == sched) {
                    schedules.splice(x, 1)
                }
            }
        }

        for (i = 0; i < schedule.length; i++) {
            var current = schedule[i].schedule;

            if (current <= '8:00 AM - 10:00 AM') {
                a--;
                if (a == 0) {
                    removeSched('8:00 AM - 10:00 AM')
                }
            } else if (current <= '10:00 AM - 12:00 PM') {
                b--;
                if (b == 0) {
                    removeSched('10:00 AM - 12:00 PM')
                }
            } else if (current <= '1:00 PM - 3:00 PM') {
                c--;
                if (c == 0) {
                    removeSched('1:00 PM - 3:00 PM');
                }
            } else if (current <= '3:00 PM - 5:00 PM') {
                d--;
                if (d == 0) {
                    removeSched('3:00 PM - 5:00 PM');
                }
            }
        }

        return schedules;
    }
}
module.exports = UC_findSchedule;