const { makeCartobuy } = require('../../entities/cartobuy/app');

const UC_createCartobuy = ({ cartobuyDb }) => {
    return async function createCartobuy(cartobuyInfo) {
        const cartoBuyEntity = makeCartobuy(cartobuyInfo);

        const check = await cartobuyDb.findCar(cartobuyInfo.carid)
        if (check.length != 0) {
            throw new Error("Car is already reserved");
        }

        await cartobuyDb.changeAvail(cartobuyInfo.carid, "RESERVED");
        console.log('asd');

        return cartobuyDb.addCartobuy(
            cartoBuyEntity.getCarid(),
            cartoBuyEntity.getCustomernumber(),
            cartoBuyEntity.getDateofreq(),
            cartoBuyEntity.getSchedule()
        )
    }
}

module.exports = UC_createCartobuy;