const UC_availableMechanics = ({ mechanicDb }) => {
    return async function availableMechanics() {
        return mechanicDb.availableMechanics()
    }
}

module.exports = UC_availableMechanics;