const { updateMechanic } = require('../../entities/mechanic/app');

const UC_updateMechanic = ({ mechanicDb }) => {
    return async function _updatemechanic(mechanicInfo){
        const mechanicEntity = await updateMechanic(mechanicInfo);

        const data = {
            id: mechanicEntity.getId(),
            employeeid: mechanicEntity.getEmployeeid(),
            serviceticketid: mechanicEntity.getServiceticketid(),
            status: mechanicEntity.getStatus(),
            priority: mechanicEntity.getPriority()
        }

        await mechanicDb.updateMechanic({...data})

        return data;
    }
}

module.exports = UC_updateMechanic;