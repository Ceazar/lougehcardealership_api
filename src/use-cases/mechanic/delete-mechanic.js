const UC_deleteMechanic = ({ mechanicDb })  => {
    return async function deleteMechanic(id) {
        const mechanic = await mechanicDb.findMechanic(id)
        const deletemechanic = await mechanicDb.deleteMechanic(id)
        if (!mechanic || Object.entries(mechanic).length == 0) {
            throw new Error("Mechanic does not exist.")
        }
        return deletemechanic, { Message: "Mechanic has been removed.", Data: mechanic }
    }
}

module.exports = UC_deleteMechanic