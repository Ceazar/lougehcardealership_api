const UC_viewMechanic = ({ mechanicDb }) => {
    return async function viewMechanic() {
        return mechanicDb.viewMechanic()
    }
}

module.exports = UC_viewMechanic;