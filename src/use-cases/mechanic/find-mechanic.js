const UC_findMechanic = ({ mechanicDb }) => {
    return async function findMechanic(id) {
        const decimal =  id.indexOf(".") == -1;
        if((!(Number.isInteger(Number(id)))) || id <= 0 || !decimal){
            throw new Error("Invalid Mechanic ID.");
        }

        const mechanic = await mechanicDb.findMechanic(id);
        
        if (!mechanic || Object.entries(mechanic).length == 0) {
            throw new Error("Mechanic does not exist.")
        }
        return mechanic;
    }
}

module.exports = UC_findMechanic;