const { makeMechanic } = require("../../entities/mechanic/app");

const UC_createMechanic = ({ mechanicDb }) => {
    return async function createMechanic(mechanicInfo) {
        const check = await mechanicDb.findMechanicbyEmpid(
            mechanicInfo.employeeid
        );

        if (check.length != 0) {
            throw new Error("Employee already exists.");
        }

        const mechanicEntity = makeMechanic(mechanicInfo);

        return mechanicDb.createMechanic(
            mechanicEntity.getEmployeeid(),
            mechanicEntity.getServiceticketid(),
            mechanicEntity.getStatus(),
            mechanicEntity.getPriority()
        );
    };
};

module.exports = UC_createMechanic;
