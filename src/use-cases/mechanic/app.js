const mechanicDb = require('../../data-access/sequelize-access/mechanic/app');

const UC_createMechanic = require('./create-mechanic');
const UC_deleteMechanic = require('./delete-mechanic');
const UC_findMechanic = require('./find-mechanic');
const UC_updateMechanic = require('./update-mechanic');
const UC_viewMechanic = require('./view-mechanic');
const UC_availableMechanics = require('./available-mechanic');

const createMechanic = UC_createMechanic({ mechanicDb });
const deleteMechanic = UC_deleteMechanic({ mechanicDb });
const findMechanic = UC_findMechanic({ mechanicDb });
const updateMechanic = UC_updateMechanic({ mechanicDb });
const viewMechanic = UC_viewMechanic({ mechanicDb });
const availableMechanics = UC_availableMechanics({ mechanicDb });

const mechanicService = Object.freeze({
    createMechanic,
    deleteMechanic,
    findMechanic,
    updateMechanic,
    viewMechanic,
    availableMechanics
});

module.exports = mechanicService;
module.exports = {
    createMechanic,
    deleteMechanic,
    findMechanic,
    updateMechanic,
    viewMechanic,
    availableMechanics
}