const partDb = require('../../data-access/sequelize-access/parts/app');

const UC_createPart = require('./create-part');
const UC_deletePart = require('./delete-part');
const UC_findPart = require('./find-part');
const UC_updatePart = require('./update-part');
const UC_viewParts = require('./view-part');

const createPart = UC_createPart({ partDb });
const deletePart = UC_deletePart({ partDb });
const findPart = UC_findPart({ partDb });
const updatePart = UC_updatePart({ partDb });
const viewParts = UC_viewParts({ partDb });

const partService = Object.freeze({
    createPart,
    deletePart,
    findPart,
    updatePart,
    viewParts
});

module.exports = partService;
module.exports = {
    createPart,
    deletePart,
    findPart,
    updatePart,
    viewParts
}