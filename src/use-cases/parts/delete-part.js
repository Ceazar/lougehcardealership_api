const UC_deletePart = ({ partDb }) => {
    return async function deletePart(id) {
        const part = await partDb.findPart(id);
        const deletepart = await partDb.deletePart(id);
        if (!part || Object.entries(part).length == 0) {
            throw new Error("Data does not exist");
        }
        return deletepart, { Message: "Part has been deleted.", Data: part };
    }
}

module.exports = UC_deletePart;