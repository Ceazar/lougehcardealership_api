const { makePart } = require('../../entities/parts/app');

const UC_createPart = ({ partDb }) => {
    return async function createPart(partInfo) {
        const partEntity = makePart(partInfo);

        return partDb.createPart(
            partEntity.getPartname(),
            partEntity.getDescription(),
            partEntity.getStock(),
            partEntity.getPrice()
        )
    }
}

module.exports = UC_createPart;