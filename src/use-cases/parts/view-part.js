const UC_getPart = ({ partDb }) => {
    return async function getParts() {
        return partDb.getParts();
    }
}

module.exports = UC_getPart;