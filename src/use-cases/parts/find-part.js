const UC_findPart = ({ partDb }) => {
    return async function findPart(id) {
        const decimal = id.indexOf(".") == -1;
        if (!Number.isInteger(Number(id)) || id <= 0 || !decimal) {
            throw new Error("Invalid Part ID.");
        }
        const part = await partDb.findPart(id);

        if (!part || part.length == 0) {
            throw new Error("Part ID does not exist.");
        }
        return part;
    };
};

module.exports = UC_findPart;
