const { updatePart } = require('../../entities/parts/app');

const UC_updatePart = ({ partDb }) => {
    return async function _updatepart(partInfo){
        const partEntity = await updatePart(partInfo);

        const data = {
            id: partEntity.getId(),
            partname: partEntity.getPartname(),
            description: partEntity.getDescription(),
            stock: partEntity.getStock(),
            price: partEntity.getPrice()
        }

        await partDb.updatePart({...data})

        return data;
    }
}

module.exports = UC_updatePart;