const UC_deleteService = ({ serviceDb }) => {
    return async function deleteService(id) {
        const service = await serviceDb.findService(id);
        const deleteservice = await serviceDb.deleteService(id);
        if (!service || Object.entries(service).length == 0) {
            throw new Error("Data does not exist.");
        }
        return deleteservice, { Message: "Successfully deleted data.", Data: service };
    }
}
module.exports = UC_deleteService;