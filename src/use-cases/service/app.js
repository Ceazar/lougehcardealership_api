const serviceDb = require('../../data-access/sequelize-access/service/app');

const UC_getService = require('./view-services');
const UC_findService = require('./find-service');
const UC_createService = require('./create-service');
const UC_updateService = require('./update-service');
const UC_deleteService = require('./delete-service');
const UC_uploadService = require('./upload-service');

const getService = UC_getService({ serviceDb });
const findService = UC_findService({ serviceDb });
const createService = UC_createService({ serviceDb });
const updateService = UC_updateService({ serviceDb });
const deleteService = UC_deleteService({ serviceDb });
const uploadService = UC_uploadService({ serviceDb });

const serviceService = Object.freeze({
    getService,
    findService,
    createService,
    updateService,
    deleteService,
    uploadService
});

module.exports = serviceService;
module.exports = {
    getService,
    findService,
    createService,
    updateService,
    deleteService,
    uploadService
}