const UC_findService = ({ serviceDb }) => {
    return async function findService(id) {
        // Check if ID has no decimals and a positive integer
        const decimal =  id.indexOf(".") == -1;
        if((!(Number.isInteger(Number(id)))) || id <= 0 || !decimal){
            throw new Error("Invalid Service ID.");
        }
        const service = await serviceDb.findService(id);
        if (!service) {
            throw new Error("Service does not exist.")
        }
        if (Object.entries(service).length == 0) {
            throw new Error("Service does not exist.");
        }
       
        return service;
    }
}

module.exports = UC_findService;