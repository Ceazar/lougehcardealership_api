const { makeService } = require("../../entities/services/app");

const UC_createService = ({ serviceDb }) => {
    return async function createService(serviceInfo) {
        const serviceEntity = makeService(serviceInfo);
        console.log(serviceInfo.servicename);
        const service = await serviceDb.getServicebyname(
            serviceInfo.servicename
        );
        if (service.length != 0) {
            throw new Error("Service already exists.");
        }

        return serviceDb.createService(
            serviceEntity.getServicename(),
            serviceEntity.getDescription(),
            serviceEntity.getPricerate(),
            serviceEntity.getDuration(),
            serviceEntity.getMechanics()
        );
    };
};

module.exports = UC_createService;
