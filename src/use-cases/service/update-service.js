const { updateService } = require("../../entities/services/app");

const UC_updateService = ({ serviceDb }) => {
    return async function _updateService(serviceInfo) {
        const serviceEntity = await updateService(serviceInfo);

        const service = await serviceDb.findService(serviceInfo.id);

        if (service.length == 0) {
            throw new Error("Service doesnt exists.");
        }

        const data = {
            id: serviceEntity.getId(),
            servicename: serviceEntity.getServicename(),
            description: serviceEntity.getDescription(),
            pricerate: serviceEntity.getPricerate(),
            duration: serviceEntity.getDuration(),
            mechanics: serviceEntity.getMechanics()
        };

        await serviceDb.updateService({ ...data });

        return data;
    };
};

module.exports = UC_updateService;
