const UC_getServices = ({ serviceDb }) => {
    return async function getServices(){
        return serviceDb.getServices();
    }
}
module.exports = UC_getServices;