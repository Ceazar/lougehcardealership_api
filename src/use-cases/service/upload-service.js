const { uploadService } = require('../../entities/services/app');

const UC_uploadService = ({ serviceDb }) => {
    return async function _uploadService(serviceDetails) {

        const searchService = await serviceDb.findService(serviceDetails.id);
        if (Object.entries(searchService).length == 0) {
          throw new Error("Service ID does not exist.")
        }
        const uploadEntity = await uploadService(serviceDetails);

        const data = {
            id: uploadEntity.getServiceid(),
            image: uploadEntity.getImage()
        }

        await serviceDb.uploadService({...data});

        return data;
    }
}

module.exports = UC_uploadService;