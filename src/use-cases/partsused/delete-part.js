const UC_deletePartused = ({ partsusedDb, serviceticketDb }) => {
    return async function deletePartused(id) {
        const partUseddata = await partsusedDb.findPartsusedByid(id);

        if (partUseddata.length == 0) {
            throw new Error("Parts used does not exist.")
        }

        const servicenumber = partUseddata[0].servicenumber;
        const quantity = partUseddata[0].quantity;
        const partData = await partsusedDb.getServicedata(partsusedEntity.getPartid());
        const serviceticketInfo = await serviceticketDb.findServicenumber(servicenumber);
        const partPrice = partData[0].price;
        const fee = partPrice * quantity;

        const oldFee = serviceticketInfo[0].servicefee;

        await serviceticketDb.updateFee(servicenumber, oldFee, fee, 'SUBTRACT');
        await partsusedDb.updatePart(id, quantity);
        await partsusedDb.deletePartused(id);

        return { message: "Partused deleted." }

    }
}

module.exports = UC_deletePartused;