const UC_getPartused = ({ partsusedDb }) => {
    return async function getPartused() {
        return partsusedDb.getPartused();
    };
};

module.exports = UC_getPartused;
