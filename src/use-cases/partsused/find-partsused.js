const UC_findPartsused = ({ partsusedDb }) => {
    return async function getPartsused(serviceticketid) {
        const decimal = serviceticketid.indexOf(".") == -1;
        if ((!(Number.isInteger(Number(serviceticketid)))) || serviceticketid <= 0 || !decimal) {
            throw new Error("Invalid Service Ticket ID.");
        }
        console.log(serviceticketid);
        const partsused = await partsusedDb.findPartsused(serviceticketid);

        if (!partsused) {
            throw new Error("Data does not exist.");
        }
        if (Object.entries(partsused).length == 0) {
            throw new Error("Data does not exist.")
        }

        return partsused;

    }
}

module.exports = UC_findPartsused;