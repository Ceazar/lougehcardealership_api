const partsusedDb = require("../../data-access/sequelize-access/partsused/app");
const serviceticketDb = require("../../data-access/sequelize-access/serviceticket/app");

const UC_findPartsused = require("./find-partsused");
const UC_findPartsusedByid = require("./findbyid-partsused");
const UC_createPartsused = require("./create-partsused");
const UC_deletePartsused = require("./delete-partsused");
const UC_updatePartsused = require("./update-partsused");
const UC_getPartused = require("./view-partsused");

const findPartsused = UC_findPartsused({ partsusedDb });
const findPartsusedByid = UC_findPartsusedByid({ partsusedDb });
const createPartsused = UC_createPartsused({ partsusedDb, serviceticketDb });
const updatePartsused = UC_updatePartsused({ partsusedDb });
const deletePartsused = UC_deletePartsused({ partsusedDb, serviceticketDb });
const getPartused = UC_getPartused({ partsusedDb });

const partsusedService = Object.freeze({
    findPartsused,
    findPartsusedByid,
    createPartsused,
    updatePartsused,
    deletePartsused,
    getPartused
});

module.exports = partsusedService;
module.exports = {
    findPartsused,
    findPartsusedByid,
    createPartsused,
    updatePartsused,
    deletePartsused,
    getPartused
};
