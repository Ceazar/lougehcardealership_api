const UC_deletePartsused = ({ partsusedDb, serviceticketDb }) => {
    return async function deletePartsused(id) {
        const partsused = await partsusedDb.findPartsusedByid(id);
        const deletepart = await partsusedDb.deletePartsused(id);
        if (!partsused || Object.entries(partsused).length == 0) {
            throw new Error("Parts used does not exist.");
        }

        if (partsused.length == 0) {
            throw new Error("Parts used does not exist.")
        }

        const servicenumber = partsused[0].servicenumber;
        const quantity = partsused[0].quantity;
        const partID = partsused[0].partid;
        const partData = await partsusedDb.getServicedata(partID);
        const serviceticketInfo = await serviceticketDb.findServicenumber(servicenumber);
        const partPrice = partData[0].price;
        const fee = partPrice * quantity;

        const oldFee = serviceticketInfo[0].servicefee;

        await serviceticketDb.updateFee(servicenumber, oldFee, fee, 'SUBTRACT');
        await partsusedDb.updatePartquantity(partID, quantity, 'ADD');

        return deletepart, { Message: "Successfully deleted data.", Data: partsused };
    }
}
module.exports = UC_deletePartsused;