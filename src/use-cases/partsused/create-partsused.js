const { makePartsused } = require('../../entities/partsused/app');

const UC_createPartsused = ({ partsusedDb, serviceticketDb }) => {
    return async function _createPartsused(partsusedInfo) {
        const partsusedEntity = makePartsused(partsusedInfo);

        await partsusedDb.updatePart(partsusedEntity.getPartid(), partsusedEntity.getQuantity());

        // console.log(await serviceticketDb.findServicenumber('1569813088931'));

        const check = await serviceticketDb.findServicenumber(partsusedEntity.getSTId());
        if (!check || check.length == 0) {
            throw new Error("Service ticket does not exists.")
        }

        const partData = await partsusedDb.getServicedata(partsusedEntity.getPartid());
        if (!partData || partData.length == 0) {
            throw new Error("Part does not exists.")
        }

        const partPrice = partData[0].price;
        const oldFee = (check[0].servicefee) * partsusedEntity.getQuantity();

        await serviceticketDb.updateFee(partsusedEntity.getSTId(), oldFee, partPrice, 'ADD');
        // await partsusedDb.updatePartquantity(partsusedEntity.getPartid(), partsusedEntity.getQuantity(), 'SUBTRACT')

        return partsusedDb.createPartsused(
            partsusedEntity.getPartid(),
            partsusedEntity.getSTId(),
            partsusedEntity.getQuantity()
        )
    }
}

module.exports = UC_createPartsused;