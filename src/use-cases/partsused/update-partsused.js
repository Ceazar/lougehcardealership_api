const { updatePartsused } = require('../../entities/partsused/app');

const UC_updatePartsused = ({ partsusedDb }) => {
    return async function _updatePartsused(partsusedInfo) {
        const partsusedEntity = await updatePartsused(partsusedInfo);

        const data = {
            id: partsusedInfo.id,
            partid: partsusedEntity.getPartid(),
            serviceticketid: partsusedEntity.getSTId(),
            quantity: partsusedEntity.getQuantity()
        }

        await partsusedDb.updatePartsused({ ...data });

        return data;
    }
}
module.exports = UC_updatePartsused;