const UC_findPartsusedByid = ({ partsusedDb }) => {
    return async function getPartsusedByid(serviceticketid){
        const partsused = await partsusedDb.findPartsusedByid(serviceticketid);
        
        if (!partsused) {
            throw new Error("ID has no match.");
        }
        if (Object.entries(partsused).length == 0) {
            throw new Error("ID has no match.")
        }

        return partsused;

    }
}

module.exports = UC_findPartsusedByid;