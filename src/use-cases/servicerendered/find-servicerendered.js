const UC_findServicerendered = ({ servicerenderedDb }, { serviceticketDb }, { serviceDb }, { employeeDb }) => {
    return async function findServicerendered(servicenumber) {

        const servicerendered = await servicerenderedDb.findServicerendered(servicenumber);
        // console.log(servicerendered);
        if (!servicerendered) {
            throw new Error("Service rendered does not exist.")
        }

        let mechanics = [];
        for (let i = 0; i < servicerendered.length; i++) {
            const ticket = await serviceticketDb.findServicenumber(servicerendered[i].dataValues.servicenumber);
            if (ticket) {
                servicerendered[i].dataValues.dateofappt = ticket[0].dataValues.dateofappt;
                servicerendered[i].dataValues.schedule = ticket[0].dataValues.starttime + " - " + ticket[0].dataValues.endtime;

                const servicename = await serviceDb.findService(servicerendered[i].dataValues.serviceid)
                servicerendered[i].dataValues.servicename = servicename[0].dataValues.servicename;
                servicerendered[i].dataValues.servicefee = servicename[0].dataValues.pricerate;

                const mechanic = await employeeDb.findEmployee(servicerendered[i].dataValues.employeeid);

                mechanics.push(mechanic[0].dataValues.fname + " " + mechanic[0].dataValues.mi + " " + mechanic[0].dataValues.lname)

            }

        }

        servicerendered[0].dataValues.mechanics = mechanics;


        return servicerendered;
    }
}

module.exports = UC_findServicerendered;