const UC_findServicerenderedmechanic = ({ servicerenderedDb }, { serviceticketDb }, { serviceDb }, { employeeDb }, { customerDb }) => {
    return async function findServicerenderedmechanic(employeeid) {

        const servicerendered = await servicerenderedDb.findAssigned(employeeid);
        if (!servicerendered) {
            throw new Error("Service rendered does not exist.")
        }

        for (let i = 0; i < servicerendered.length; i++) {
            const serviceticket = await serviceticketDb.findServicenumber(servicerendered[i].dataValues.servicenumber);

            const customer = await customerDb.findCustomerNumber(serviceticket[0].dataValues.customernumber);

            servicerendered[i].dataValues.customer = customer[0].dataValues;
            servicerendered[i].dataValues.serviceticket = serviceticket[0].dataValues;

            const service = await serviceDb.findService(servicerendered[i].dataValues.serviceid);
            servicerendered[i].dataValues.service = service[0].dataValues;

        }

        return servicerendered
    }
}

module.exports = UC_findServicerenderedmechanic;