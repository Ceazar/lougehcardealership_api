const serviceticketDb = require('../../data-access/sequelize-access/serviceticket/app');
const servicerenderedDb = require('../../data-access/sequelize-access/servicerendered/app');
const serviceDb = require('../../data-access/sequelize-access/service/app');
const employeeDb = require('../../data-access/sequelize-access/employees/app');
const customerDb = require('../../data-access/sequelize-access/customers/app');


const UC_findServicerendered = require('./find-servicerendered');
const UC_findServicerenderedmechanic = require('./find-mech-services');

const findServicerendered = UC_findServicerendered({ servicerenderedDb }, { serviceticketDb }, { serviceDb }, { employeeDb });
const findServicerenderedmechanic = UC_findServicerenderedmechanic({ servicerenderedDb }, { serviceticketDb }, { serviceDb }, { employeeDb }, { customerDb });


const servicerenderedService = Object.freeze({
    findServicerendered,
    findServicerenderedmechanic
});

module.exports = servicerenderedService;
module.exports = {
    findServicerendered,
    findServicerenderedmechanic
}