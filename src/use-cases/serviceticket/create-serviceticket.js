const { makeServiceticket } = require('../../entities/serviceticket/app');
const moment = require('moment');

const UC_createServiceticket = ({ serviceticketDb }) => {
    return async function createServiceticket(serviceticketInfo) {
        console.log(serviceticketInfo);

        // Check if date is not before today
        var as = moment(serviceticketInfo.dateofappt, 'YYYY-MM-DD');
        var curtime = moment().format('YYYY-MM-DD');

        if (as.isBefore(curtime)) {
            throw new Error("Invalid date. Please input a date not before today.")
        }

        // Get available mechanics
        const avmech = await serviceticketDb.availableMechanics();
        if (avmech == 0) {
            throw new Error("No available mechanics at the moment. Please try again later.")
        }

        // Get service Information
        const getService = await serviceticketDb.serviceDuration(serviceticketInfo.serviceid);
        // Get the average duration of the service
        const minduration = getService[0].duration;
        // Get required mechanic for the service
        const minmech = getService[0].mechanics;
        // Get service fee
        const fee = getService[0].pricerate;

        // Get mechanic schedules
        const mechaniclist = await serviceticketDb.getAllmechanics();
        var mechanicSchedules = {};

        for (var bb = 0; bb < mechaniclist.length; bb++) {
            var getSchedules = await serviceticketDb.getSchedules(serviceticketInfo.dateofappt);

            var currentmechanic = mechaniclist[bb].employeeid;
            var ms = [];

            for (var cc = 0; cc < getSchedules.length; cc++) {
                var checker = await serviceticketDb.checkSched(currentmechanic, getSchedules[cc].servicenumber);
                if (Object.entries(checker).length != 0) {
                    ms.push(getSchedules[cc].endtime)
                }
            }
            if (ms.length == 0) {
                ms.push('8:00 AM')
            }
            mechanicSchedules[currentmechanic] = ms;
        }

        var mechanicid = mechaniclist[0].employeeid;
        var len = mechanicSchedules[mechanicid].length;
        var currentSched = mechanicSchedules[mechanicid][len - 1];
        var availableEmployees = [];

        // Get earliest sched
        for (bb = 0; bb < mechaniclist.length; bb++) {
            var mechanicid = mechaniclist[bb].employeeid;
            var len = mechanicSchedules[mechanicid].length;
            var toSched = mechanicSchedules[mechanicid][len - 1];
            timea = moment(currentSched, 'hh:mm A');
            timeb = moment(toSched, 'hh:mm A')

            // console.log('a=',timea.format('hh:mm A'), 'b=',timeb.format('hh:mm A'));

            if (timeb.isBefore(timea)) {
                var avm = 0;
                for (var j = 0; j < mechaniclist.length; j++) {
                    var mechanicid2 = mechaniclist[j].employeeid;
                    var len2 = mechanicSchedules[mechanicid2].length;
                    var toSched2 = mechanicSchedules[mechanicid2][len2 - 1];

                    var timec = moment(toSched2, 'hh:mm A');
                    if (timec.isSameOrBefore(timeb)) {
                        avm++;
                    }
                }
                if (avm >= minmech) {
                    currentSched = toSched;
                }
            }
        }

        var timea = moment(currentSched, 'hh:mm A');
        var timeb = moment('12:00 PM', 'hh:mm A')

        // Lunch Break :D
        if (timea.isSame(timeb)) {
            currentSched = '1:00 PM';
        }

        // Get Available Employees
        for (bb = 0; bb < mechaniclist.length; bb++) {
            var mechanicid = mechaniclist[bb].employeeid;
            var len = mechanicSchedules[mechanicid].length;
            var schedule = mechanicSchedules[mechanicid][len - 1];
            var timeb = moment(schedule, 'hh:mm A')

            if (timeb.isSameOrBefore(timea)) {
                availableEmployees.push(mechanicid);
            }
        }

        // Check if service will not be done until 5:00 PM
        var timeb = moment('5:00 PM', 'hh:mm A');

        if (timea.isSameOrAfter(timeb)) {
            throw new Error("Services are only until 5:00 PM. Please choose another date.")
        }

        var appointment = moment(currentSched, 'hh:mm A');
        // Add service duration to starttime
        appointment.add(minduration, 'm');

        // Insert into the object   
        serviceticketInfo.starttime = currentSched;
        serviceticketInfo.endtime = appointment.format('hh:mm A');
        serviceticketInfo.servicefee = fee;
        serviceticketInfo.servicenumber = Date.now();

        // Mechanic Auto assigning 
        const priorities = await serviceticketDb.mechanicbyPriority();
        var havework = [];
        index = 0;
        var allocated = 0;

        for (var index = 0; index < priorities.length; index++) {
            var mechanicid = priorities[index].employeeid;

            if (allocated < minmech) {
                // console.log(allocated, availableEmployees);

                for (bb = 0; bb < availableEmployees.length; bb++) {
                    var avaMech = availableEmployees[bb];

                    if (mechanicid == avaMech) {
                        havework.push(mechanicid);

                        serviceticketDb.createRendered(
                            serviceticketInfo.serviceid,
                            serviceticketInfo.servicenumber,
                            mechanicid
                        );

                        allocated++;
                        bb = availableEmployees.length;

                        var a_priority = ((avmech - minmech) + (priorities[index].priority));
                        await serviceticketDb.updatePriority(mechanicid, a_priority);
                    }
                }
            }
            else if (allocated >= minmech) {
                var b_priority = (priorities[index].priority) - minmech;
                await serviceticketDb.updatePriority(mechanicid, b_priority);
            }

        }

        serviceticketInfo.mechanics = havework;

        // Create Entity
        const serviceticketEntity = await makeServiceticket(serviceticketInfo);

        await serviceticketDb.createServiceticket(
            serviceticketEntity.getServicenumber(),
            serviceticketEntity.getCustomernumber(),
            serviceticketEntity.getDateofappt(),
            serviceticketEntity.getStarttime(),
            serviceticketEntity.getEndtime(),
            serviceticketEntity.getStatus(),
            serviceticketEntity.getServicefee(),
            serviceticketEntity.getMechanics()
        )

        return serviceticketInfo;

    }
}

module.exports = UC_createServiceticket;