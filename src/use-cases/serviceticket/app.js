const serviceticketDb = require('../../data-access/sequelize-access/serviceticket/app');
const servicerenderedDb = require('../../data-access/sequelize-access/servicerendered/app');
const serviceDb = require('../../data-access/sequelize-access/service/app');

const UC_createServiceticket = require('./create-serviceticket');
const UC_findServiceticket = require('./find-serviceticket');
const UC_updateServiceticket = require('./update-serviceticket');
const UC_getServiceticket = require('./view-serviceticket');
const UC_deleteServiceticket = require('./delete-serviceticket');

const createServiceticket = UC_createServiceticket({ serviceticketDb });
const findServiceticket = UC_findServiceticket({ serviceticketDb });
const updateServiceticket = UC_updateServiceticket({ serviceticketDb });
const getServiceticket = UC_getServiceticket({ serviceticketDb }, { servicerenderedDb }, { serviceDb });
const deleteServiceticket = UC_deleteServiceticket({ serviceticketDb });

const serviceticketService = Object.freeze({
    createServiceticket,
    findServiceticket,
    updateServiceticket,
    getServiceticket,
    deleteServiceticket
});

module.exports = serviceticketService;
module.exports = {
    createServiceticket,
    findServiceticket,
    updateServiceticket,
    getServiceticket,
    deleteServiceticket
}