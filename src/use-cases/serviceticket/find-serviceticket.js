const UC_findServiceticket = ({ serviceticketDb }) => {
    return async function findServiceticket(id) {
        const decimal =  id.indexOf(".") == -1;
        if((!(Number.isInteger(Number(id)))) || id <= 0 || !decimal){
            throw new Error("Invalid Service ticket ID.");
        }
        const serviceticket = await serviceticketDb.findServiceticket(id);
        // console.log(serviceticket);
        if (!serviceticket) {
            throw new Error("Service ticket does not exist.")
        }
        if (Object.entries(serviceticket).length == 0) {
            throw new Error("Service ticket does not exist.");
        }
        return serviceticket;
    }
}

module.exports = UC_findServiceticket;