const e = require("express");

const UC_getServiceticket = ({ serviceticketDb }, { servicerenderedDb }, { serviceDb }) => {
    return async function getServiceticket(id) {
        if (id) {
            const serviceticket = await serviceticketDb.findServiceticketbyEmployee(id);

            for (let i = 0; i < serviceticket.length; i++) {
                const serviceredendered = await servicerenderedDb.findServicerendered(serviceticket[i].dataValues.servicenumber);
                const service = await serviceDb.findService(serviceredendered[0].dataValues.serviceid);
                serviceticket[i].dataValues.servicename = service[0].dataValues.servicename
            }
            return serviceticket
        }
        return serviceticketDb.getServiceticket();
    }
}

module.exports = UC_getServiceticket;