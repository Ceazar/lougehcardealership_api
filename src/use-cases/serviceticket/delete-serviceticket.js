const UC_deleteServiceticket = ({ serviceticketDb }) => {
    return async function deleteServiceticket(servicenumber) {
        const cservicenumber = await serviceticketDb.findServicenumber(servicenumber);
        // const deleteserviceticket = await serviceticketDb.deleteServiceticket(id);
        if (!cservicenumber || Object.entries(cservicenumber).length == 0) {
            throw new Error("Service number does not exist.");
        }

        const usedmechanics = await serviceticketDb.findEmployeeswithSched(servicenumber);
        const mechanics = await serviceticketDb.getAllmechanics();
        const mechlen = mechanics.length;
        const usedlen = usedmechanics.length;
        var oldPriority = 0;
        var newPriority = 0;

        for (var i = 0; i < mechanics.length; i++) {
            var mechanic = mechanics[i].employeeid;

            oldPriority = await serviceticketDb.getPrioritynumber(mechanic);
            newPriority = oldPriority[0].priority + usedlen;

            if (newPriority > mechlen) {
                newPriority = newPriority - mechlen;
            }

            await serviceticketDb.updatePriority(mechanic, newPriority);
        }

        await serviceticketDb.CancelSched(servicenumber);

        // return deleteserviceticket, { Message: "Successfully deleted data.", Data: serviceticket };
    }
}
module.exports = UC_deleteServiceticket;