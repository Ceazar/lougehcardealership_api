const { updateServiceticket } = require("../../entities/serviceticket/app");

const UC_updateServiceticket = ({ serviceticketDb }) => {
    return async function _updateServiceticket(serviceticketInfo) {
        const serviceticketEntity = await updateServiceticket(
            serviceticketInfo
        );

        const data = {
            id: serviceticketEntity.getId(),
            customernumber: serviceticketEntity.getCustomernumber(),
            serviceid: serviceticketEntity.getServiceid(),
            servicefee: serviceticketEntity.getServicefee(),
            status: serviceticketEntity.getStatus()
        };
        await serviceticketDb.updateServiceticket({ ...data });

        return data;
    };
};

module.exports = UC_updateServiceticket;
