const { updateSalesperson } = require("../../entities/salesperson/app");

const UC_updateSalesperson = ({ salespersonDb }) => {
    return async function _updatesalesperson(salespersonInfo) {
        const salespersonEntity = await updateSalesperson(salespersonInfo);

        const check = await salespersonDb.findSalesperson(salespersonInfo.id);
        if (check.length == 0) {
            throw new Error("Salesperson does not exist.");
        }

        const data = {
            id: salespersonEntity.getId(),
            employeeid: salespersonEntity.getEmployeeid(),
            status: salespersonEntity.getStatus(),
            priority: salespersonEntity.getPriority()
        };

        await salespersonDb.updateSalesperson({ ...data });

        return data;
    };
};

module.exports = UC_updateSalesperson;
