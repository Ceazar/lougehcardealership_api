const { makeSalesperson } = require('../../entities/salesperson/app')

const UC_createSalesperson = ({ salespersonDb }) => {
    return async function createSalesperson(salespersonInfo) {
        const salespersonData = await salespersonDb.countSalesperson();
        const priority = salespersonData + 1;

        salespersonInfo.priority = priority;

        const salespersonEntity = makeSalesperson(salespersonInfo)

        // await salespersonDb.updatePriority(priority)

        return salespersonDb.createSalesperson(
            salespersonEntity.getEmployeeid(),
            salespersonEntity.getStatus(),
            salespersonEntity.getPriority()
        )
    }
}

module.exports = UC_createSalesperson