const salespersonDb = require('../../data-access/sequelize-access/salesperson/app');

const UC_createSalesperson = require('./create-salesperson');
const UC_deleteSalesperson = require('./delete-salesperson');
const UC_findSalesperson = require('./find-salesperson');
const UC_updateSalesperson = require('./update-salesperson');
const UC_viewSalesperson = require('./view-salesperson');

const createSalesperson = UC_createSalesperson({ salespersonDb });
const deleteSalesperson = UC_deleteSalesperson({ salespersonDb });
const findSalesperson = UC_findSalesperson({ salespersonDb });
const updateSalesperson = UC_updateSalesperson({ salespersonDb });
const viewSalesperson = UC_viewSalesperson({ salespersonDb });

const salespersonService = Object.freeze({
    createSalesperson,
    deleteSalesperson,
    findSalesperson,
    updateSalesperson,
    viewSalesperson
});

module.exports = salespersonService;
module.exports = {
    createSalesperson,
    deleteSalesperson,
    findSalesperson,
    updateSalesperson,
    viewSalesperson
}