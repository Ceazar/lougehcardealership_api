const UC_viewSalesperson = ({ salespersonDb }) => {
    return async function viewSalesperson() {
        return salespersonDb.viewSalesperson()
    }
}

module.exports = UC_viewSalesperson;