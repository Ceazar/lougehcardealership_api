const UC_findSalesperson = ({ salespersonDb }) => {
    return async function findSalesperson(id) {
        const decimal =  id.indexOf(".") == -1;
        if((!(Number.isInteger(Number(id)))) || id <= 0 || !decimal){
            throw new Error("Invalid Salesperson ID.");
        }

        const salesperson = await salespersonDb.findSalesperson(id);
        
        if (!salesperson || Object.entries(salesperson).length == 0) {
            throw new Error("Salesperson does not exist.")
        }
        return salesperson;
    }
}

module.exports = UC_findSalesperson;