const UC_deleteSalesperson = ({ salespersonDb }) => {
    return async function deleteSalesperson(id) {
        const salesperson = await salespersonDb.findSalesperson(id)
        const deletesalesperson = await salespersonDb.deleteSalesperson(id)
        if (!salesperson || Object.entries(salesperson).length == 0) {
            throw new Error("Salesperson does not exist.")
        }
        const priority = salesperson[0].priority;
        await salespersonDb.updatePriority(priority);
        return deletesalesperson, { Message: "Salesperson has been removed.", Data: salesperson }
    }
}

module.exports = UC_deleteSalesperson