const carDb = require('../../data-access/sequelize-access/cars/app');

const viewCarsUseCase = require('./view-cars');
const createCarUseCase = require('./create-car');
const findCarUseCase = require('./find-car');
const UC_updateCar = require('./update-car');
const UC_uploadCar = require('./upload-car');
const UC_carstock = require('./stocks-car');

const getCars = viewCarsUseCase({ carDb });
const addCar = createCarUseCase({ carDb });
const findCar = findCarUseCase({ carDb });
const updateCar = UC_updateCar({ carDb });
const uploadCar = UC_uploadCar({ carDb });
const countCar = UC_carstock({ carDb });

const carService = Object.freeze({
    getCars,
    addCar,
    findCar,
    updateCar,
    uploadCar,
    countCar
});

module.exports = carService;
module.exports = {
    getCars,
    addCar,
    findCar,
    updateCar,
    uploadCar,
    countCar
}