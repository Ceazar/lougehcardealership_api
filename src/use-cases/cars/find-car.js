const findCarUseCase = ({ carDb }) => {
    return async function findCar(vin, req) {
        const car = await carDb.findCar(vin);

        if (!car) {
            throw new Error("Car ID does not match any car on database.");
        }
        if (Object.entries(car).length == 0) {
            throw new Error("Car ID does not match any car on database.")
        }

        if (req.getColor) {
            const colors = await carDb.getCarColors(car[0].dataValues);

            let colorsArray = [];

            for (let i = 0; i < colors.length; i++) {
                colorsArray.push(colors[i].dataValues)
            }
            car[0].dataValues.colors = colorsArray

        }
        return car


    }
}

module.exports = findCarUseCase;