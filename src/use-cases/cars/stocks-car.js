const UC_carstock = ({ carDb }) => {
    return async function getCarstock(model) {
        const count = await carDb.getCarstock(model);
        if (!model) {
            throw new Error("Model missing.")
        }

        return count
    }
}

module.exports = UC_carstock;