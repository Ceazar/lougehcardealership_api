const { makeCar } = require('../../entities/cars/app');

const createCaruseCase = ({ carDb }) => {
    return async function createCar(carInfo) {
        const carEntity = makeCar(carInfo);

        const check = await carDb.findVin(carEntity.getVin());

        if (Object.entries(check).length != 0) {
            throw new Error("Provide a unique VIN.");
        }

        return carDb.addCar(
            carEntity.getVin(),
            carEntity.getModel(),
            carEntity.getCompany(),
            carEntity.getColor(),
            carEntity.getStatus(),
            carEntity.getPrice(),
            carEntity.getAvailabile(),
            carInfo.horsepower,
            carInfo.transmission,
            carInfo.enginedisplacement,
            carInfo.fueltype
        )

    }
}

module.exports = createCaruseCase;