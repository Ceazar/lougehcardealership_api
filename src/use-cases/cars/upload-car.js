const { uploadCar } = require('../../entities/cars/app')

const UC_uploadCar = ({ carDb }) => {
    return async function _uploadCar(carDetails) {

      const searchCar = await carDb.findCarbyid(carDetails.id);
      if (Object.entries(searchCar).length == 0) {
        throw new Error("Car ID does not exist.")
      }

      const uploadEntity = await uploadCar(carDetails);

      const data = {
        id: uploadEntity.getCarid(),
        carimage: uploadEntity.getImage()
      }

      await carDb.uploadCar({...data});

      return data;    
      }
}

module.exports = UC_uploadCar;

