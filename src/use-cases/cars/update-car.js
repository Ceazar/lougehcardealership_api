const { updateCar } = require('../../entities/cars/app');

const UC_updateCar = ({ carDb }) => {
    return async function _updateCar(carInfo) {
        const carEntity = await updateCar(carInfo);

        const check = await carDb.findVin(carEntity.getVin());

        const checkCar = await carDb.findCar(carInfo.id);

        if (checkCar.length == 0) {
            throw new Error("Car does not exist.")
        }

        if (Object.entries(check).length != 0 && checkCar[0].dataValues.vin != carEntity.getVin()) {
            throw new Error("Provide a unique VIN.");
        }

        const data = {
            id: carEntity.getId(),
            vin: carEntity.getVin(),
            model: carEntity.getModel(),
            company: carEntity.getModel(),
            color: carEntity.getColor(),
            status: carEntity.getStatus(),
            price: carEntity.getPrice(),
            available: carEntity.getAvailabile(),
            horsepower: carInfo.horsepower,
            transmission: carInfo.transmission,
            enginedisplacement: carInfo.enginedisplacement,
            fueltype: carInfo.fueltype
        }

        await carDb.updateCar({ ...data });

        return data;
    }
}

module.exports = UC_updateCar;