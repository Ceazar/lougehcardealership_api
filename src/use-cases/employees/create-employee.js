const { makeEmployee } = require('../../entities/employees/app');
const e = require('express');

const createEmployeeUseCase = ({ employeeDb }, { accountDb }, { mechanicDb }, { salespersonDb }) => {
    return async function createEmployee(empInfo) {

        console.log(empInfo);

        const employeeEntity = makeEmployee(empInfo);

        const check = await employeeDb.findEmployee(employeeEntity.getEmployeeId());

        // console.log(check);

        if (Object.entries(check).length != 0) {
            throw new Error("Employee ID exists");
        }

        if (empInfo.position == 'Mechanic') {
            const priority = await mechanicDb.findLastPriority();

            let curprio;

            if (priority[0]) {
                curprio = priority[0].dataValues.priority + 1;
            } else {
                curprio = 1;
            }

            await mechanicDb.createMechanic(empInfo.employeeid, 'AVAILABLE', curprio)
        } else if (empInfo.position == 'Salesperson') {
            const priority = await salespersonDb.orderbyPriority();

            let curprio;

            if (priority[0]) {
                curprio = priority[0].dataValues.priority + 1;
            } else {
                curprio = 1;
            }

            await salespersonDb.createSalesperson(empInfo.employeeid, 'AVAILABLE', curprio)

        }

        // Auto Generate Employee ID
        await accountDb.createAccount(
            employeeEntity.getEmployeeId(),
            employeeEntity.getUsername(),
            employeeEntity.getPassword(),
            employeeEntity.getPosition()
        )

        return employeeDb.createemployee(
            employeeEntity.getEmployeeId(),
            employeeEntity.getFname(),
            employeeEntity.getMi(),
            employeeEntity.getLname(),
            employeeEntity.getPosition(),
            employeeEntity.getAge(),
            employeeEntity.getPhone(),
            employeeEntity.getEmail()
        );
    }
}

module.exports = createEmployeeUseCase;