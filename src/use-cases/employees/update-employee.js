const { makeupdateEmployee } = require('../../entities/employees/app');

const updateEmployeeUseCase = ({ employeeDb }) => {
    return async function updateEmployee(empInfo) {
        if (empInfo.deleteEmployee) {
            if (!empInfo.employeeid) {
                throw new Error('Employee ID is required.')
            }
            await employeeDb.deleteEmployee(empInfo.employeeid);

            return

        } else {
            const employeeEntity = await makeupdateEmployee(empInfo);

            const check = await employeeDb.findEmployee(empInfo.employeeid);
            if (!check || check.length == 0) {
                throw new Error("Employee does not exist.")
            }

            const data = {
                employeeid: employeeEntity.getEmployeeId(),
                fname: employeeEntity.getFname(),
                mi: employeeEntity.getMi(),
                lname: employeeEntity.getLname(),
                position: employeeEntity.getPosition(),
                age: employeeEntity.getAge(),
                phone: employeeEntity.getPhone(),
                email: employeeEntity.getEmail()
            }


            await employeeDb.updateEmployee({ ...data });


            return data;
        }

    }
}

module.exports = updateEmployeeUseCase;