const employeeDb = require('../../data-access/sequelize-access/employees/app');
const accountDb = require('../../data-access/sequelize-access/accounts/app');
const mechanicDb = require('../../data-access/sequelize-access/mechanic/app');
const salespersonDb = require('../../data-access/sequelize-access/salesperson/app');

const createEmployeeUseCase = require('./create-employee');
const viewEmployeeUseCase = require('./view-employees');
const updateEmployeeUseCase = require('./update-employee');
const findEmployeeUseCase = require('./find-employee');
const UC_createEmpid = require('./create-empid');

const createEmployee = createEmployeeUseCase({ employeeDb }, { accountDb }, { mechanicDb }, { salespersonDb });
const getEmployees = viewEmployeeUseCase({ employeeDb });
const updateEmployee = updateEmployeeUseCase({ employeeDb });
const findEmployee = findEmployeeUseCase({ employeeDb });
const createEmpid = UC_createEmpid({ employeeDb });

const empService = Object.freeze({
    createEmployee,
    getEmployees,
    updateEmployee,
    findEmployee,
    createEmpid
});

module.exports = empService;
module.exports = {
    createEmployee,
    getEmployees,
    updateEmployee,
    findEmployee,
    createEmpid
}