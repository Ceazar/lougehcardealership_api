const findEmployeeUseCase = ({ employeeDb }) => {
    return async function findEmployee(employeeid) {
        const employee = await employeeDb.findEmployee(employeeid);
        if (!employee) {
            throw new Error("Employee does not exist.");
        }
        if (Object.entries(employee).length == 0){
            throw new Error("Employee does not exist.");
        }
        return employee
    }
}

module.exports = findEmployeeUseCase;