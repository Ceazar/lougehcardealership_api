const e = require("express");

const UC_createEmpid = ({ employeeDb }) => {
    return async function createEmpid(prefix) {
        if (!prefix) {
            throw new Error("Please provide a prefix.")
        }
        const empID = await employeeDb.createEmpid();

        let generated;
        if (empID[0]) {
            generated = prefix + (empID[0].dataValues.id + 1);
        } else {
            generated = prefix + '1';
        }

        return generated;
    }
}
module.exports = UC_createEmpid;