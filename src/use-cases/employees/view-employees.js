const useCaseGetEmployees = ({ employeeDb }) => {
    return async function getEmployees(){
        return employeeDb.getEmployees();
    }
}

module.exports = useCaseGetEmployees;