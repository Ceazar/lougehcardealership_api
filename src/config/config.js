require("dotenv").config();

module.exports = {
    development: {
        username: "postgres",
        password: "postgres",
        database: "lougehcar",
        host: "localhost",
        dialect: "postgres",
    },
    test: {
        username: "postgres",
        password: "postgres",
        database: "lougehcar_test",
        host: "127.0.0.1",
        dialect: "postgres"
    },
    // production: {
    //   username: "postgres",
    //   password: "postgres",
    //   database: "lougehcar_prod",
    //   host: "127.0.0.1",
    //   dialect: "postgres"
    // },
    secretKey: "sickwet"
};
