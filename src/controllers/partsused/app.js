const CON_createPartsused = require("./partsused-create");
const CON_deletePartsused = require("./partsused-delete");
const CON_findPartsused = require("./partsused-find");
const CON_updatePartsused = require("./partsused-update");
const CON_viewPartsused = require("./partsused-view");

const {
    createPartsused,
    findPartsused,
    deletePartsused,
    updatePartsused,
    getPartused
} = require("../../use-cases/partsused/app");

const _createPartsused = CON_createPartsused({ createPartsused });
const _deletePartsused = CON_deletePartsused({ deletePartsused });
const _findPartsused = CON_findPartsused({ findPartsused });
const _updatePartsused = CON_updatePartsused({ updatePartsused });
const _viewPartsused = CON_viewPartsused({ getPartused });

const CON_partsUsed = Object.freeze({
    _createPartsused,
    _deletePartsused,
    _findPartsused,
    _updatePartsused,
    _viewPartsused
});

module.exports = CON_partsUsed;
module.exports = {
    _createPartsused,
    _findPartsused,
    _deletePartsused,
    _updatePartsused,
    _viewPartsused
};
