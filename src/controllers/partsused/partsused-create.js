const CON_createPartsused = ({ createPartsused }) => {
    return async function createPartsusedd(httpRequest){

        try {
            const { source = {}, ...partsusedInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }

            const createdPartsused = await createPartsused(partsusedInfo);

            return {
                headers: {
                    "Constent-Type": "application/json",

                },
                statusCode: 201,
                body: {
                    message: "Part used added.",
                    PartInfo: { createdPartsused }
                }
            }

        } catch (err) {
            // console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
}
module.exports = CON_createPartsused;