const CON_viewMechanic = require('./mechanic-view');
const CON_findMechanic = require('./mechanic-find');
const CON_createMechanic = require('./mechanic-create');
const CON_updateMechanic = require('./mechanic-update');
const CON_deleteMechanic = require('./mechanic-delete');
const CON_availableMechanics = require('./mechanic-available');
const {
    viewMechanic,
    findMechanic,
    createMechanic,
    updateMechanic,
    deleteMechanic,
    availableMechanics
} = require('../../use-cases/mechanic/app');

const _viewMechanic = CON_viewMechanic({ viewMechanic });
const _findMechanic = CON_findMechanic({ findMechanic });
const _createMechanic = CON_createMechanic({ createMechanic });
const _updateMechanic = CON_updateMechanic({ updateMechanic });
const _deleteMechanic = CON_deleteMechanic({ deleteMechanic });
const _availableMechanics = CON_availableMechanics({ availableMechanics });

const CON_mechanic = Object.freeze({
    _viewMechanic,
    _findMechanic,
    _createMechanic,
    _updateMechanic,
    _deleteMechanic,
    _availableMechanics
});

module.exports = CON_mechanic;
module.exports = {
    _viewMechanic,
    _findMechanic,
    _createMechanic,
    _updateMechanic,
    _deleteMechanic,
    _availableMechanics
}