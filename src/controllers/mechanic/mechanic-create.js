const CON_createMechanic = ({ createMechanic }) => {
    return async function create(httpRequest) {

        try {
            const { source = {}, ...mechanicInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }

            // console.log(serviceInfo);
            const createdMechanic = await createMechanic(mechanicInfo);

            return {
                headers: {
                    "Constent-Type": "application/json",

                },
                statusCode: 201,
                body: {
                    message: "Mechanic added.",
                    mechanicInfo: { createdMechanic }
                }
            }

        } catch (err) {
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
}
module.exports = CON_createMechanic;