const CON_uploadcar = ({ uploadCar }) => {
    return async function upload(httpRequest) {
        try {
            const { source = {}, ...carInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }

            const filename = httpRequest.fullUrl + httpRequest.file.filename;

            const carDetails = {
                id: httpRequest.params.id,
                filename,
                ...carInfo
            };

            // console.log("filename", carDetails);
            const uploadedCar = await uploadCar(carDetails);

            return {
                headers: {
                    "Constent-Type": "application/json",

                },
                statusCode: 200,
                body: {
                    message: "Car image has been uploaded.",
                    carimage: { uploadedCar }
                }
            }

        } catch (err) {
            // console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }

    }
}

module.exports = CON_uploadcar;