const controllerFindCars = ({ findCar, countCar }) => {
  return async function findCarss(httpRequest) {
    try {
      //get the httprequest body
      const { source = {}, ...data } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      const toView = {
        ...data,
        source,
        vin: httpRequest.params.vin
      };
      // end here; to retrieve id

      const car = await findCar(toView.vin, data);
      const model = (car[0].model);
      const stocks = await countCar(model);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 200,
        body: { data: car, stocks }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      if (e.name === "RangeError") {
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 404,
          body: {
            error: e.message
          }
        };
      }
      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 400,
        body: {
          error: e.message

        }
      };
    }
  };
};


module.exports = controllerFindCars;
