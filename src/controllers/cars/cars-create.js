const controllerCreateCar = ({ addCar }) => {
    return async function addCarr(httpRequest) {
        try {
            const { source = {}, ...carInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }

            const createdCar = await addCar(carInfo);

            return {
                headers: {
                    "Constent-Type": "application/json",

                },
                statusCode: 201,
                body: {
                    message: "Car has been added.",
                    carInfo: { createdCar }
                }
            }

        } catch (err) {
            // console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }

    }
}

module.exports = controllerCreateCar;