const controllerViewCars = require('./cars-view');
const controllerFindCars = require('./cars-find');
const controllerAddCar = require('./cars-create');
const CON_updateCar = require('./cars-update');
const CON_uploadCar = require('./cars-upload');

const {
    getCars,
    findCar,
    addCar,
    updateCar,
    uploadCar,
    countCar
} = require('../../use-cases/cars/app');

const ViewCars = controllerViewCars({ getCars });
const FindCar = controllerFindCars({ findCar, countCar });
const AddCar = controllerAddCar({ addCar });
const UpdateCar = CON_updateCar({ updateCar });
const UploadCar = CON_uploadCar({ uploadCar });

const carController = Object.freeze({
    ViewCars,
    FindCar,
    AddCar,
    UpdateCar,
    UploadCar
})

module.exports = carController;
module.exports = {
    ViewCars,
    FindCar,
    AddCar,
    UpdateCar,
    UploadCar
}