const CON_createCustomer = ({ createCustomer }) => {
    return async function createCustomerr(httpRequest) {

        try {
            const { source = {}, ...customerInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }

            const createdCustomer = await createCustomer(customerInfo);

            return {
                headers: {
                    "Constent-Type": "application/json",

                },
                statusCode: 201,
                body: {
                    message: "Customer added.",
                    customerInfo: { createdCustomer }
                }
            }

        } catch (err) {
            // console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }

    }
}

module.exports = CON_createCustomer;
