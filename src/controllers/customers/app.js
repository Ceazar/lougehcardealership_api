const CON_getCustomers = require('./customer-view');
const CON_findCustomer = require('./customer-find');
const CON_createCustomer = require('./customer-create');
const CON_updateCustomer = require('./customer-update');

const {
    getCustomers,
    findCustomer,
    createCustomer,
    updateCustomer
} = require('../../use-cases/customers/app');

const _getCustomer = CON_getCustomers({ getCustomers });
const _findCustomer = CON_findCustomer({ findCustomer });
const _createCustomer = CON_createCustomer({ createCustomer });
const _updateCustomer = CON_updateCustomer({ updateCustomer });

const CON_customer = Object.freeze({
    _getCustomer,
    _findCustomer,
    _createCustomer,
    _updateCustomer
});

module.exports = CON_customer;
module.exports = {
    _getCustomer,
    _findCustomer,
    _createCustomer,
    _updateCustomer
}