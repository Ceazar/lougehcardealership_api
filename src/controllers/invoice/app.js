const CON_getInvoice = require('./invoice-view');
const CON_findInvoice = require('./invoice-find');
const CON_createInvoice = require('./invoice-create');
const CON_updateInvoice = require('./invoice-update');

const {
    getInvoice,
    findInvoice,
    createInvoice,
    updateInvoice
} = require('../../use-cases/invoice/app');

const _getInvoice = CON_getInvoice({ getInvoice });
const _findInvoice = CON_findInvoice({ findInvoice });
const _createInvoice = CON_createInvoice({ createInvoice });
const _updateInvoice = CON_updateInvoice({ updateInvoice });

const CON_invoice = Object.freeze({
    _getInvoice,
    _findInvoice,
    _createInvoice,
    _updateInvoice
});

module.exports = CON_invoice;
module.exports = {
    _getInvoice,
    _findInvoice,
    _createInvoice,
    _updateInvoice
}