const CON_createInvoice = ({ createInvoice }) => {
    return async function post(httpRequest) {

        try {
            const { source = {}, ...invoiceInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }

            // console.log(invoiceInfo);
            const createdInvoice = await createInvoice(invoiceInfo);

            return {
                headers: {
                    "Constent-Type": "application/json",

                },
                statusCode: 201,
                body: {
                    message: "Invoice added.",
                    invoiceInfo: { createdInvoice }
                }
            }

        } catch (err) {
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
}
module.exports = CON_createInvoice;