const CON_updateInvoice = ({ updateInvoice }) => {
    return async function put(httpRequest){
        try {
            // get http request to submitted data
            const id = httpRequest.params.id;
            const { source = {} , ...invoiceInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-Agent"];
            if (httpRequest.headers["Referer"]) {
              source.referrer = httpRequest.headers["Referer"];
            }
            // end
            const toEdit = {
                id,
              ...invoiceInfo
            }
            
            // console.log(toEdit)
            const data = await updateInvoice(toEdit);

            // console.log(patched);
            return {
              headers: {
                'Content-Type': 'application/json'
              },
              statusCode: 200,
              body: { data }
            }
            
          } catch (e) {
            // TODO: Error logging
            console.log(e);
      
            return {
              headers: {
                "Content-Type": "application/json"
              },
              statusCode: 400,
              body: {
                error: e.message
              }
            };
          }
    }
}

module.exports = CON_updateInvoice;

