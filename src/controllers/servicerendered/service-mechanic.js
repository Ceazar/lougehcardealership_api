const CON_findServicerenderedmechanic = ({ findServicerenderedmechanic }) => {
    return async function findServiceerendered(httpRequest) {
        try {
            //get the httprequest body
            const { source = {}, ...servicerenderedInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-Agent"];
            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }
            const toView = {
                ...servicerenderedInfo,
                source,
                id: httpRequest.params.id
            };
            // end here; to retrieve id

            const data = await findServicerenderedmechanic(toView.id);
            return {
                headers: {
                    "Content-Type": "application/json"
                },
                statusCode: 200,
                body: { data }
            };
        } catch (e) {
            // TODO: Error logging
            console.log(e);
            if (e.name === "RangeError") {
                return {
                    headers: {
                        "Content-Type": "application/json"
                    },
                    statusCode: 404,
                    body: {
                        error: e.message
                    }
                };
            }
            return {
                headers: {
                    "Content-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: e.message

                }
            };
        }
    };
};


module.exports = CON_findServicerenderedmechanic;
