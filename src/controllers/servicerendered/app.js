
const CON_findServicerendered = require('./servicerendered-find');
const CON_findServicerenderedmechanic = require('./service-mechanic');


const {
    findServicerendered,
    findServicerenderedmechanic
} = require('../../use-cases/servicerendered/app');


const _findServicerendered = CON_findServicerendered({ findServicerendered });
const _findServicerenderedmechanic = CON_findServicerenderedmechanic({ findServicerenderedmechanic });


const CON_servicerendered = Object.freeze({
    _findServicerendered,
    _findServicerenderedmechanic
});

module.exports = CON_servicerendered;
module.exports = {
    _findServicerendered,
    _findServicerenderedmechanic
}