const CON_createSalesperson = ({ createSalesperson }) => {
    return async function create(httpRequest) {

        try {
            const { source = {}, ...salespersonInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }

            // console.log(serviceInfo);
            const createdSalesperson = await createSalesperson(salespersonInfo);

            return {
                headers: {
                    "Constent-Type": "application/json",

                },
                statusCode: 201,
                body: {
                    message: "Salesperson added.",
                    salespersonInfo: { createdSalesperson }
                }
            }

        } catch (err) {
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
}
module.exports = CON_createSalesperson;