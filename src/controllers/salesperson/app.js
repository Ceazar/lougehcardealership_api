const CON_viewSalesperson = require('./salesperson-view');
const CON_findSalesperson = require('./salesperson-find');
const CON_createSalesperson = require('./salesperson-create');
const CON_updateSalesperson = require('./salesperson-update');
const CON_deleteSalesperson = require('./salesperson-delete');
const {
    viewSalesperson,
    findSalesperson,
    createSalesperson,
    updateSalesperson,
    deleteSalesperson
} = require('../../use-cases/salesperson/app');

const _viewSalesperson = CON_viewSalesperson({ viewSalesperson });
const _findSalesperson = CON_findSalesperson({ findSalesperson });
const _createSalesperson = CON_createSalesperson({ createSalesperson });
const _updateSalesperson = CON_updateSalesperson({ updateSalesperson });
const _deleteSalesperson = CON_deleteSalesperson({ deleteSalesperson });

const CON_salesperson = Object.freeze({
    _viewSalesperson,
    _findSalesperson,
    _createSalesperson,
    _updateSalesperson,
    _deleteSalesperson
});

module.exports = CON_salesperson;
module.exports = {
    _viewSalesperson,
    _findSalesperson,
    _createSalesperson,
    _updateSalesperson,
    _deleteSalesperson
}