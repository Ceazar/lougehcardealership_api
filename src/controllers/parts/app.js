const CON_getPart = require('./part-view');
const CON_findPart = require('./part-find');
const CON_createPart = require('./part-create');
const CON_updatePart = require('./part-update');
const CON_deletePart = require('./part-delete');
const {
    viewParts,
    findPart,
    createPart,
    updatePart,
    deletePart
} = require('../../use-cases/parts/app');

const _getPart = CON_getPart({ viewParts });
const _findPart = CON_findPart({ findPart });
const _createPart = CON_createPart({ createPart });
const _updatePart = CON_updatePart({ updatePart });
const _deletePart = CON_deletePart({ deletePart });

const CON_part = Object.freeze({
    _getPart,
    _findPart,
    _createPart,
    _updatePart,
    _deletePart
});

module.exports = CON_part;
module.exports = {
    _getPart,
    _findPart,
    _createPart,
    _updatePart,
    _deletePart
}