const CON_createPart = ({ createPart }) => {
    return async function create(httpRequest) {

        try {
            const { source = {}, ...partInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }

            // console.log(serviceInfo);
            const createdPart = await createPart(partInfo);

            return {
                headers: {
                    "Constent-Type": "application/json",

                },
                statusCode: 201,
                body: {
                    message: "Part info added.",
                    partInfo: { createdPart }
                }
            }

        } catch (err) {
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
}
module.exports = CON_createPart;