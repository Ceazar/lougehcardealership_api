const CON_findCartobuy = ({ findCartobuy }) => {
    return async function findCarss(httpRequest) {
      try {
        //get the httprequest body
        const { source = {}, ...cartobuyInfo } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
        const toView = {
          ...cartobuyInfo,
          source,
          id: httpRequest.params.id
        };
        // end here; to retrieve id
  
        const data = await findCartobuy(toView.id);
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 200,
          body: { data }
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        if (e.name === "RangeError") {
          return {
            headers: {
              "Content-Type": "application/json"
            },
            statusCode: 404,
            body: {
              error: e.message
            }
          };
        }
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 400,
          body: {
            error: e.message
            
          }
        };
      }
    };
  };
  
  
  module.exports = CON_findCartobuy;
  