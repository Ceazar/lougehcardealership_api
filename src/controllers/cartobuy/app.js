const CON_createCartobuy = require('./cartobuy-create');
const CON_updateCartobuy = require('./cartobuy-update');
const CON_deleteCartobuy = require('./cartobuy-delete');
const CON_viewCartobuy = require('./cartobuy-view');
const CON_findCartobuy = require('./cartobuy-find');
const CON_findSchedule = require('./find-schedule');

const {
    createCartobuy,
    updateCartobuy,
    deleteCartobuy,
    viewCartobuy,
    findCartobuy,
    findSchedule
} = require('../../use-cases/cartobuy/app');

const _createCartobuy = CON_createCartobuy({ createCartobuy });
const _updateCartobuy = CON_updateCartobuy({ updateCartobuy });
const _deleteCartobuy = CON_deleteCartobuy({ deleteCartobuy });
const _viewCartobuy = CON_viewCartobuy({ viewCartobuy });
const _findCartobuy = CON_findCartobuy({ findCartobuy });
const _findSchedule = CON_findSchedule({ findSchedule });

const CON_cartobuy = Object.freeze({
    _createCartobuy,
    _updateCartobuy,
    _deleteCartobuy,
    _viewCartobuy,
    _findCartobuy,
    _findSchedule
});

module.exports = CON_cartobuy;
module.exports = {
    _createCartobuy,
    _updateCartobuy,
    _deleteCartobuy,
    _viewCartobuy,
    _findCartobuy,
    _findSchedule
}