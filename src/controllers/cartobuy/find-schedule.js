const CON_findSchedule = ({ findSchedule }) => {
    return async function Carstoget(httpRequest) {
      try {
        //get the httprequest body
        const { source = {}, dateofreq } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
        
        const view = await findSchedule(dateofreq);
        
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 200,
          body: { AvailableSchedules: view }
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        if (e.name === "RangeError") {
          return {
            headers: {
              "Content-Type": "application/json"
            },
            statusCode: 404,
            body: {
              error: e.message
            }
          };
        }
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 400,
          body: {
            error: e.message
            
          }
        };
      }
    };
  };
  
  
  module.exports = CON_findSchedule;
  