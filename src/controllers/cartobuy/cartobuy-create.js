const CON_createCar = ({ createCartobuy }) => {
    return async function post(httpRequest) {

        try {
            const { source = {}, ...cartobuyInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }

            // console.log(cartobuyInfo);

            const createdCartobuy = await createCartobuy(cartobuyInfo);

            return {
                headers: {
                    "Constent-Type": "application/json",

                },
                statusCode: 201,
                body: {
                    message: "Request sent.",
                    cartobuyInfo: { createdCartobuy }
                }
            }

        } catch (err) {
            // console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }

    }
}

module.exports = CON_createCar;