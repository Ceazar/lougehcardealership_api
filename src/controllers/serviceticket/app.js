const CON_getserviceticket = require('./serviceticket-view');
const CON_findServiceticket = require('./serviceticket-find');
const CON_createServiceticket = require('./serviceticket-create');
const CON_updateServiceticket = require('./serviceticket-update');
const CON_deleteServiceticket = require('./serviceticket-delete');

const {
    getServiceticket,
    findServiceticket,
    createServiceticket,
    updateServiceticket,
    deleteServiceticket
} = require('../../use-cases/serviceticket/app');

const _getServiceticket = CON_getserviceticket({ getServiceticket });
const _findServiceticket = CON_findServiceticket({ findServiceticket });
const _createServiceticket = CON_createServiceticket({ createServiceticket });
const _updateServiceticket = CON_updateServiceticket({ updateServiceticket });
const _deleteServiceticket = CON_deleteServiceticket({ deleteServiceticket });

const CON_serviceticket = Object.freeze({
    _getServiceticket,
    _findServiceticket,
    _createServiceticket,
    _updateServiceticket,
    _deleteServiceticket
});

module.exports = CON_serviceticket;
module.exports = {
    _getServiceticket,
    _findServiceticket,
    _createServiceticket,
    _updateServiceticket,
    _deleteServiceticket
}