const CON_createServiceticket = ({ createServiceticket }) => {
    return async function createServiceeticket(httpRequest) {

        try {
            const { source = {}, ...serviceticketInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }

            // console.log(serviceticketInfo);
            const Schedule = await createServiceticket(serviceticketInfo);

            return {
                headers: {
                    "Constent-Type": "application/json",

                },
                statusCode: 201,
                body: {
                    message: "Service added.",
                    serviceticketInfo: { Schedule }
                }
            }

        } catch (err) {
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
}
module.exports = CON_createServiceticket;