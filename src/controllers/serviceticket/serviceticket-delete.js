const CON_deleteServiceticket = ({ deleteServiceticket }) => {
  return async function put(httpRequest) {
    try {
      // get http request to submitted data
      const servicenumber = httpRequest.params.servicenumber;
      const { source = {}, ...serviceticketInfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      // end
      const toEdit = {
        servicenumber,
        ...serviceticketInfo
      }

      // console.log(toEdit)
      const data = await deleteServiceticket(toEdit.servicenumber);

      // console.log(patched);
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 200,
        body: { Message: "Service has been canceled." }
      }

    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  }
}

module.exports = CON_deleteServiceticket;

