const jwt = require("../../infra/jwt/app");

const CON_loginAccount = ({ loginAccount }) => {
  return async function findAccountt(httpRequest) {
    try {
      //get the httprequest body
      const { source = {}, ...accountInfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      const accountDetails = {
        ...accountInfo
      };
      // end here; to retrieve id

      // console.log(accountDetails);

      const account = await loginAccount(accountDetails);
      const token = await jwt.generateToken(account);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 200,
        body: {
          Account: account,
          token
        }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      if (e.name === "RangeError") {
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 404,
          body: {
            error: e.message
          }
        };
      }
      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = CON_loginAccount;
