const CON_getAccounts = require('./accounts-view');
const CON_findAccount = require('./accounts-find');
const CON_createAccount = require('./accounts-create');
const CON_updateAccount = require('./accounts-update');
const CON_deleteAccount = require('./accounts-delete');
const CON_loginAccount = require('./accounts-login');

const {
    getAccounts,
    findAccount,
    createAccount,
    updateAccount,
    deleteAccount,
    loginAccount
} = require('../../use-cases/accounts/app');

const _getAccounts = CON_getAccounts({ getAccounts });
const _findAccount = CON_findAccount({ findAccount });
const _createAccount = CON_createAccount({ createAccount });
const _updateAccount = CON_updateAccount({ updateAccount });
const _deleteAccount = CON_deleteAccount({ deleteAccount });
const _loginAccount = CON_loginAccount({ loginAccount });

const CON_account = Object.freeze({
    _getAccounts,
    _findAccount,
    _createAccount,
    _updateAccount,
    _deleteAccount,
    _loginAccount
});

module.exports = CON_account;
module.exports = {
    _getAccounts,
    _findAccount,
    _createAccount,
    _updateAccount,
    _deleteAccount,
    _loginAccount
}