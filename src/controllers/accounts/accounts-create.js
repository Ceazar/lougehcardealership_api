const CON_createAccount = ({ createAccount }) => {
  return async function createAccountt(httpRequest) {

    try {
      const { source = {}, ...accountInfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-agent"];

      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      await createAccount(accountInfo);

      return {
        headers: {
          "Constent-Type": "application/json",

        },
        statusCode: 201,
        body: {
          message: "Account has been created."
        }
      }

    } catch (err) {
      // console.log(err);
      return {
        headers: {
          "Constent-Type": "application/json"
        },
        statusCode: 400,
        body: {
          error: err.message
        }
      }
    }
  }
}
module.exports = CON_createAccount;