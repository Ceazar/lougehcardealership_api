const CON_uploadservice = ({ uploadService }) => {
    return async function upload(httpRequest) {
        try {
            const { source = {}, id, ...serviceInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }

            const filename = httpRequest.fullUrl + httpRequest.file.filename;

            const serviceDetails = {
                filename,
                id: httpRequest.params.id,
                ...serviceInfo
            };

            // console.log("filename", carDetails);
            const uploadedService = await uploadService(serviceDetails);

            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 200,
                body: {
                    message: "Service image has been uploaded.",
                    Serviceimage: { uploadedService }
                }
            };
        } catch (err) {
            // console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };
        }
    };
};

module.exports = CON_uploadservice;
