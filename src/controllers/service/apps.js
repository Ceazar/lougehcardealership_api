const CON_getservice = require('./service-view');
const CON_findService = require('./service-find');
const CON_createService = require('./service-create');
const CON_updateService = require('./service-update');
const CON_deleteService = require('./service-delete');
const CON_uploadService = require('./service-upload');

const {
    getService,
    findService,
    createService,
    updateService,
    deleteService,
    uploadService
} = require('../../use-cases/service/app');

const _getService = CON_getservice({ getService });
const _findService = CON_findService({ findService });
const _createService = CON_createService({ createService });
const _updateService = CON_updateService({ updateService });
const _deleteService = CON_deleteService({ deleteService });
const _uploadService = CON_uploadService({ uploadService });

const CON_service = Object.freeze({
    _getService,
    _findService,
    _createService,
    _updateService,
    _deleteService,
    _uploadService
});

module.exports = CON_service;
module.exports = {
    _getService,
    _findService,
    _createService,
    _updateService,
    _deleteService,
    _uploadService
}