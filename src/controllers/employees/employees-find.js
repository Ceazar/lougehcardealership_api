const controllerFindEmployees = ({ findEmployee }) => {
    return async function findEmployeess(httpRequest) {
      try {
        //get the httprequest body
        const { source = {}, ...empInfo } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
        const toView = {
          ...empInfo,
          source,
          employeeid: httpRequest.params.employeeid
        };
        // end here; to retrieve id
  
        const data = await findEmployee(toView.employeeid);
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 200,
          body: { data }
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        if (e.name === "RangeError") {
          return {
            headers: {
              "Content-Type": "application/json"
            },
            statusCode: 404,
            body: {
              error: e.message
            }
          };
        }
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 400,
          body: {
            error: e.message
            
          }
        };
      }
    };
  };
  
  
  module.exports = controllerFindEmployees;
  