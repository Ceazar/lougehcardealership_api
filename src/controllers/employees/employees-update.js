const controllerUpdateEmployee = ({ updateEmployee }) => {
  return async function put(httpRequest) {
    try {
      // get http request to submitted data
      const employeeid = httpRequest.params.employeeid;
      const { source = {}, ...empinfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      // end
      const toEdit = {
        employeeid,
        ...empinfo
      }

      // console.log(toEdit)
      const data = await updateEmployee(toEdit);

      // console.log(patched);
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 200,
        body: { data }
      }

    } catch (e) {
      console.log(e);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  }
}

module.exports = controllerUpdateEmployee;

