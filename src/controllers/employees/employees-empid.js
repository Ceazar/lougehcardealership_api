const CON_createEmpid = ({ createEmpid }) => {
  return async function createEmpidd(httpRequest) {
    try {
      //get the httprequest body
      const { source = {}, prefix, ...empInfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      console.log(httpRequest.body);

      const toView = {
        ...empInfo,
        source,
        prefix
      };
      // end here; to retrieve id
      // const EmployeeID = "15415151";


      const view = await createEmpid(toView.prefix);

      // console.log(view[0]);
      const EmployeeID = (view);


      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 200,
        body: { "newempid": EmployeeID }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      if (e.name === "RangeError") {
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 404,
          body: {
            error: e.message
          }
        };
      }
      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 400,
        body: {
          error: e.message

        }
      };
    }
  };
};


module.exports = CON_createEmpid;
