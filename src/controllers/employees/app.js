const controllerCreateEmployee = require('./employees-create');
const controllerViewEmployees = require('./employees-view');
const controllerUpdateEmployee = require('./employees-update');
const controllerFindEmployees = require('./employees-find');
const CON_createEmpid = require('./employees-empid');

const {
    createEmployee,
    getEmployees,
    updateEmployee,
    findEmployee,
    createEmpid
} = require('../../use-cases/employees/app');

const createNewEmployee = controllerCreateEmployee({ createEmployee });
const getAllEmployees = controllerViewEmployees({ getEmployees });
const _updateEmployee = controllerUpdateEmployee({ updateEmployee });
const _findEmployee = controllerFindEmployees({ findEmployee });
const _createEmpid = CON_createEmpid({ createEmpid });

const employeeControllers = Object.freeze({
    createNewEmployee,
    getAllEmployees,
    _updateEmployee,
    _createEmpid
});

module.exports = employeeControllers;
module.exports = {
    createNewEmployee,
    getAllEmployees,
    _updateEmployee,
    _findEmployee,
    _createEmpid
}