const controllerCreateEmployee = ({ createEmployee }) => {
    return async function addEmployee(httpRequest) {

        try {
            const { source = {}, ...empInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if (httpRequest.headers["Referer"]) {
                source.referrer = httpRequest.headers["Referer"];
            }

            console.log(empInfo);
            const createdEmployee = await createEmployee(empInfo);

            return {
                headers: {
                    "Constent-Type": "application/json",

                },
                statusCode: 201,
                body: {
                    message: "Employee has been added.",
                    employeeInfo: { createdEmployee }
                }
            }

        } catch (err) {
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }

    }
}

module.exports = controllerCreateEmployee;