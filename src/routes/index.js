var express = require("express");
var router = express.Router();

const makeExpressCallback = require("../express-callback/app");
const middleware = require("../middlewares/middleware");
const { upload } = require("../middlewares/upload");

upload.single("file");
// Employee Controllers
const {
    createNewEmployee,
    getAllEmployees,
    _updateEmployee,
    _findEmployee,
    _createEmpid
} = require("../controllers/employees/app");

//  Car Controllers
const {
    ViewCars,
    FindCar,
    AddCar,
    UpdateCar,
    UploadCar
} = require("../controllers/cars/app");

// Customer Constrollers
const {
    _getCustomer,
    _findCustomer,
    _createCustomer,
    _updateCustomer
} = require("../controllers/customers/app");

// Partsused Controllers
const {
    _createPartsused,
    _findPartsused,
    _deletePartsused,
    _updatePartsused,
    _viewPartsused
} = require("../controllers/partsused/app");

// Service Controllers
const {
    _getService,
    _findService,
    _createService,
    _updateService,
    _deleteService,
    _uploadService
} = require("../controllers/service/apps");

// Accounts
const {
    _getAccounts,
    _findAccount,
    _createAccount,
    _updateAccount,
    _deleteAccount,
    _loginAccount
} = require("../controllers/accounts/app");

// Cartobuy
const {
    _createCartobuy,
    _updateCartobuy,
    _deleteCartobuy,
    _viewCartobuy,
    _findCartobuy,
    _findSchedule
} = require("../controllers/cartobuy/app");

// Service ticket
const {
    _getServiceticket,
    _findServiceticket,
    _createServiceticket,
    _updateServiceticket,
    _deleteServiceticket
} = require("../controllers/serviceticket/app");


// Service rendered
const {
    _findServicerendered,
    _findServicerenderedmechanic
} = require("../controllers/servicerendered/app");

// Invoice
const {
    _getInvoice,
    _findInvoice,
    _createInvoice,
    _updateInvoice
} = require("../controllers/invoice/app");

// Parts
const {
    _getPart,
    _findPart,
    _createPart,
    _updatePart,
    _deletePart
} = require("../controllers/parts/app");

// Mechanic
const {
    _viewMechanic,
    _findMechanic,
    _createMechanic,
    _updateMechanic,
    _deleteMechanic,
    _availableMechanics
} = require("../controllers/mechanic/app");

// Salesperson
const {
    _viewSalesperson,
    _findSalesperson,
    _createSalesperson,
    _updateSalesperson,
    _deleteSalesperson
} = require("../controllers/salesperson/app");

// ------------------------------Routes---------------------------------

/* GET home page. */
router.get("/", function (req, res, next) {
    res.render("index", { title: "Lou Geh Car Dealership" });
});

// LOGIN
router.post("/api/accounts/login", makeExpressCallback(_loginAccount));

// Employees
router.get(
    "/api/employees/list",
    middleware.verifytoken,
    makeExpressCallback(getAllEmployees)
);
router.post(
    "/api/employees/create",
    middleware.verifytoken,
    makeExpressCallback(createNewEmployee)
);
router.post(
    "/api/employees/empid",
    middleware.verifytoken,
    makeExpressCallback(_createEmpid)
);
router.put(
    "/api/employees/:employeeid",
    middleware.verifytoken,
    makeExpressCallback(_updateEmployee)
);
router.get(
    "/api/employees/:employeeid",
    middleware.verifytoken,
    makeExpressCallback(_findEmployee)
);

// Cars
router.get(
    "/api/cars/list",
    middleware.verifytoken,
    makeExpressCallback(ViewCars)
);
router.post(
    "/api/cars/list",
    middleware.verifytoken,
    makeExpressCallback(ViewCars)
);
router.get(
    "/api/cars/:vin",
    middleware.verifytoken,
    makeExpressCallback(FindCar)
);
router.post(
    "/api/cars/find/:vin",
    middleware.verifytoken,
    makeExpressCallback(FindCar)
);
router.post(
    "/api/cars/create",
    middleware.verifytoken,
    makeExpressCallback(AddCar)
);
router.put(
    "/api/cars/:id",
    middleware.verifytoken,
    makeExpressCallback(UpdateCar)
);
router.post(
    "/api/cars/upload/:id",
    middleware.verifytoken,
    upload.single("file"),
    makeExpressCallback(UploadCar)
);

// Customer
router.get(
    "/api/customers/list",
    middleware.verifytoken,
    makeExpressCallback(_getCustomer)
);
router.get(
    "/api/customers/:id",
    middleware.verifytoken,
    makeExpressCallback(_findCustomer)
);
router.post(
    "/api/customers/create",

    makeExpressCallback(_createCustomer)
);
router.put(
    "/api/customers/:id",
    middleware.verifytoken,
    makeExpressCallback(_updateCustomer)
);

// Partsused
router.get(
    "/api/partsused/list",
    middleware.verifytoken,
    makeExpressCallback(_viewPartsused)
);
router.post(
    "/api/partsused/create",
    middleware.verifytoken,
    makeExpressCallback(_createPartsused)
);
router.get(
    "/api/partsused/:serviceticketid",
    middleware.verifytoken,
    makeExpressCallback(_findPartsused)
);
router.put(
    "/api/partsused/:id",
    middleware.verifytoken,
    makeExpressCallback(_updatePartsused)
);
router.delete(
    "/api/partsused/:id",
    middleware.verifytoken,
    makeExpressCallback(_deletePartsused)
);

// Service
router.get(
    "/api/services/list",
    middleware.verifytoken,
    makeExpressCallback(_getService)
);
router.get(
    "/api/services/:id",
    middleware.verifytoken,
    makeExpressCallback(_findService)
);
router.post(
    "/api/services/create",
    middleware.verifytoken,
    makeExpressCallback(_createService)
);
router.put(
    "/api/services/:id",
    middleware.verifytoken,
    makeExpressCallback(_updateService)
);
router.delete(
    "/api/services/:id",
    middleware.verifytoken,
    makeExpressCallback(_deleteService)
);
router.post(
    "/api/services/upload/:id",
    middleware.verifytoken,
    upload.single("file"),
    makeExpressCallback(_uploadService)
);

// Accounts
router.get(
    "/api/accounts/list",
    middleware.verifytoken,
    makeExpressCallback(_getAccounts)
);
router.get(
    "/api/accounts/:id",
    middleware.verifytoken,
    makeExpressCallback(_findAccount)
);
router.post(
    "/api/accounts/create",
    middleware.verifytoken,
    makeExpressCallback(_createAccount)
);
router.put(
    "/api/accounts/:id",
    middleware.verifytoken,
    makeExpressCallback(_updateAccount)
);
router.delete(
    "/api/accounts/:id",
    middleware.verifytoken,
    makeExpressCallback(_deleteAccount)
);

// Cartobuy
router.post(
    "/api/cartobuy/schedule",
    middleware.verifytoken,
    makeExpressCallback(_findSchedule)
);
router.get(
    "/api/cartobuy/list",
    middleware.verifytoken,
    makeExpressCallback(_viewCartobuy)
);
router.put(
    "/api/cartobuy/:id",
    middleware.verifytoken,
    makeExpressCallback(_updateCartobuy)
);
router.delete(
    "/api/cartobuy/:id",
    middleware.verifytoken,
    makeExpressCallback(_deleteCartobuy)
);
router.post(
    "/api/cartobuy/create",
    middleware.verifytoken,
    makeExpressCallback(_createCartobuy)
);
router.post(
    "/api/cartobuy/:id",
    middleware.verifytoken,
    makeExpressCallback(_findCartobuy)
);

// Service ticket
router.get(
    "/api/serviceticket/list",
    middleware.verifytoken,
    makeExpressCallback(_getServiceticket)
);
router.get(
    "/api/serviceticket/list/:id",
    middleware.verifytoken,
    makeExpressCallback(_getServiceticket)
);
router.get(
    "/api/serviceticket/:id",
    middleware.verifytoken,
    makeExpressCallback(_findServiceticket)
);
router.post(
    "/api/serviceticket/create",
    middleware.verifytoken,
    makeExpressCallback(_createServiceticket)
);
router.put(
    "/api/serviceticket/:id",
    middleware.verifytoken,
    makeExpressCallback(_updateServiceticket)
);
router.delete(
    "/api/serviceticket/:servicenumber",
    middleware.verifytoken,
    makeExpressCallback(_deleteServiceticket)
);

// Service renderd
router.get(
    "/api/servicerendered/find/:id",
    middleware.verifytoken,
    makeExpressCallback(_findServicerendered)
);
router.get(
    "/api/servicerendered/assigned/:id",
    middleware.verifytoken,
    makeExpressCallback(_findServicerenderedmechanic)
);

// Invoice
router.get(
    "/api/invoice/list",
    middleware.verifytoken,
    makeExpressCallback(_getInvoice)
);
router.get(
    "/api/invoice/:id",
    middleware.verifytoken,
    makeExpressCallback(_findInvoice)
);
router.post(
    "/api/invoice/create",
    middleware.verifytoken,
    makeExpressCallback(_createInvoice)
);
router.put(
    "/api/invoice/:id",
    middleware.verifytoken,
    makeExpressCallback(_updateInvoice)
);

// Parts
router.get(
    "/api/parts/list",
    middleware.verifytoken,
    makeExpressCallback(_getPart)
);
router.get(
    "/api/parts/:id",
    middleware.verifytoken,
    makeExpressCallback(_findPart)
);
router.post(
    "/api/parts/create",
    middleware.verifytoken,
    makeExpressCallback(_createPart)
);
router.put(
    "/api/parts/:id",
    middleware.verifytoken,
    makeExpressCallback(_updatePart)
);
router.delete(
    "/api/parts/:id",
    middleware.verifytoken,
    makeExpressCallback(_deletePart)
);

// Mechanic
router.get(
    "/api/mechanic/list",
    middleware.verifytoken,
    makeExpressCallback(_viewMechanic)
);
router.get(
    "/api/mechanic/available",
    middleware.verifytoken,
    makeExpressCallback(_availableMechanics)
);
router.get(
    "/api/mechanic/:id",
    middleware.verifytoken,
    makeExpressCallback(_findMechanic)
);
router.post(
    "/api/mechanic/create",
    middleware.verifytoken,
    makeExpressCallback(_createMechanic)
);
router.put(
    "/api/mechanic/:id",
    middleware.verifytoken,
    makeExpressCallback(_updateMechanic)
);
router.delete(
    "/api/mechanic/:id",
    middleware.verifytoken,
    makeExpressCallback(_deleteMechanic)
);

// Salesperson
router.get(
    "/api/salesperson/list",
    middleware.verifytoken,
    makeExpressCallback(_viewSalesperson)
);
router.get(
    "/api/salesperson/:id",
    middleware.verifytoken,
    makeExpressCallback(_findSalesperson)
);
router.post(
    "/api/salesperson/create",
    middleware.verifytoken,
    makeExpressCallback(_createSalesperson)
);
router.put(
    "/api/salesperson/:id",
    middleware.verifytoken,
    makeExpressCallback(_updateSalesperson)
);
router.delete(
    "/api/salesperson/:id",
    middleware.verifytoken,
    makeExpressCallback(_deleteSalesperson)
);

module.exports = router;
