const jwt = require("jsonwebtoken");
const config = require("../../config/config.js");

const jwtFunction = () => {
  return Object.freeze({
    generateToken
  });

  async function generateToken(account) {
    return jwt.sign(
      {
        uid: account.uid,
        roleid: account.roleid
      },
      config.secretKey,
      { expiresIn: "10h" }
    );
  }
};

module.exports = jwtFunction;
