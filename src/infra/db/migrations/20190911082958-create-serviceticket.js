'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Servicetickets', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      servicenumber: {
        type: Sequelize.STRING
      },
      serviceid: {
        type: Sequelize.INTEGER
      },
      customernumber: {
        type: Sequelize.STRING
      },
      dateofappt: {
        type: Sequelize.STRING
      },
      starttime: {
        type: Sequelize.STRING
      },
      endtime: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING
      },
      servicefee: {
        type: Sequelize.FLOAT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Servicetickets');
  }
};