'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Cars', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      vin: {
        type: Sequelize.STRING
      },
      horsepower: {
        type: Sequelize.STRING
      },
      transmission: {
        type: Sequelize.STRING
      },
      enginedisplacement: {
        type: Sequelize.STRING
      },
      fueltype: {
        type: Sequelize.STRING
      },
      model: {
        type: Sequelize.STRING
      },
      company: {
        type: Sequelize.STRING
      },
      color: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.FLOAT
      },
      available: {
        type: Sequelize.STRING
      },
      carimage: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Cars');
  }
};