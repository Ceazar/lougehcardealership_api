const makeServiceticketEntity = require('./serviceticket');
const updateServiceticketEntity = require('./update-serviceticket');

const makeServiceticket = makeServiceticketEntity({});
const updateServiceticket = updateServiceticketEntity({});

const serviceticketEntity = Object.freeze({
    makeServiceticket,
    updateServiceticket
});

module.exports = serviceticketEntity;
module.exports = {
    makeServiceticket,
    updateServiceticket
}