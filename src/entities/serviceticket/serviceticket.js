const makeServiceticketEntity = ({ }) => {
    return function makeServiceticket({
        servicenumber,
        customernumber,
        dateofappt,
        starttime,
        endtime,
        status,
        servicefee,
        mechanics
    }) {
        if (!servicenumber) {
            throw new Error("Service Number required.")
        }
        if (!customernumber) {
            throw new Error("Customer required.");
        }
        if (!dateofappt) {
            throw new Error("Date required.");
        }
        if (!starttime) {
            throw new Error("Start time required.")
        }
        if (!endtime) {
            throw new Error("End time required.")
        }
        if (!status) {
            status = "PENDING";
        }
        if (!mechanics) {
            mechanics = "";
        }


        return Object.freeze({
            getServicenumber: () => servicenumber,
            getCustomernumber: () => customernumber,
            getDateofappt: () => dateofappt,
            getStarttime: () => starttime,
            getEndtime: () => endtime,
            getStatus: () => status,
            getServicefee: () => servicefee,
            getMechanics: () => mechanics
        })
    }
}
module.exports = makeServiceticketEntity;