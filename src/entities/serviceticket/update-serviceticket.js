const updateServiceticketEntity = ({ }) => {
    return function updateServiceticket({
        id,
        servicenumber,
        customernumber,
        serviceid,
        servicefee,
        status
    }) {
        if (!servicenumber) {
            throw new Error("Service Number required.");
        }
        if (!customernumber) {
            throw new Error("Customer required.");
        }
        if (serviceid) {
            throw new Error("Service required.");
        }
        if (!servicefee) {
            throw new Error("Service fee required.");
        }

        if (servicefee < 0) {
            throw new Error("Invalid Service fee.");
        }

        return Object.freeze({
            getId: () => id,
            getServicenumber: () => servicenumber,
            getCustomernumber: () => customernumber,
            getServiceid: () => serviceid,
            getServicefee: () => servicefee,
            getStatus: () => status
        });
    };
};

module.exports = updateServiceticketEntity;
