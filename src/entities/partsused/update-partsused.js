const updatePartsusedEntity = ({ }) => {
    return function updatePartsused({ id, partid, servicenumber, quantity }) {
        if (!partid) {
            throw new Error("Part name required.");
        }
        if (!quantity) {
            quantity = 1;
        }
        if (!servicenumber) {
            throw new Error("servicenumber missing.")
        }

        const idd = id.indexOf(".") == -1;
        if ((!(Number.isInteger(Number(id)))) || id <= 0 || !idd) {
            throw new Error("Invalid ID.");
        }

        const decimal = servicenumber.indexOf(".") == -1;
        if ((!(Number.isInteger(Number(servicenumber)))) || servicenumber <= 0 || !decimal) {
            throw new Error("Invalid Service Ticket ID.");
        }

        return Object.freeze({
            getId: () => id,
            getPartid: () => partid,
            getSTId: () => servicenumber,
            getQuantity: () => quantity
        })
    }
}

module.exports = updatePartsusedEntity;