const makePartsusedEntity = ({ }) => {
    return function createPartsused({ partid, servicenumber, quantity }) {
        if (!partid) {
            throw new Error("Part name required.");
        }
        if (!quantity) {
            quantity = 1;
        }
        if (!servicenumber) {
            throw new Error("serviceticketid missing.")
        }

        const Stringquantity = toString(quantity);
        const decimal = Stringquantity.indexOf(".") == -1;
        if (!(Number.isInteger(Number(quantity))) || quantity <= 0 || !decimal) {
            throw new Error("Invalid Quantity.")
        }

        const Stringstid = toString(servicenumber)
        const stid = Stringstid.indexOf(".") == -1;
        if (!(Number.isInteger(Number(servicenumber))) || servicenumber <= 0 || !stid) {
            throw new Error("Invalid Service Number.")
        }

        return Object.freeze({
            getPartid: () => partid,
            getSTId: () => servicenumber,
            getQuantity: () => quantity
        })
    }
}

module.exports = makePartsusedEntity;