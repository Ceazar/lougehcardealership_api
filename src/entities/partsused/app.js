const makePartsusedEntity = require('./partsused');
const updatePartsusedEntity = require('./update-partsused');

const makePartsused = makePartsusedEntity({});
const updatePartsused = updatePartsusedEntity({});

const partsusedEntity = Object.freeze({
    makePartsused,
    updatePartsused
});

module.exports = partsusedEntity;
module.exports = {
    makePartsused,
    updatePartsused
}