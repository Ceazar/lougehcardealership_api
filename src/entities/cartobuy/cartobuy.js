const makeCartobuyEntity = ({ }) => {
    return function makeCartobuy({ carid, customernumber, dateofreq, schedule }) {
        if (!carid || !customernumber) {
            throw new Error("Empty Fields.");
        }

        if (!dateofreq || !schedule) {
            throw new Error("Date/Schedule Empty")
        }
        // const today = new Date();
        // console.log(today);

        // dateofreq = ""+ today;

        return Object.freeze({
            getCarid: () => carid,
            getCustomernumber: () => customernumber,
            getDateofreq: () => dateofreq,
            getSchedule: () => schedule
        })

    }
}

module.exports = makeCartobuyEntity;