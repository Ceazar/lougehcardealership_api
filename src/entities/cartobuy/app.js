const makeCartobuyEntity = require('./cartobuy');
const updateCartobuyEntity = require('./update-cartobuy');

const makeCartobuy = makeCartobuyEntity({});
const updateCartobuy = updateCartobuyEntity({});

const cartobuyEntity = Object.freeze({
    makeCartobuy,
    updateCartobuy
});

module.exports = cartobuyEntity;
module.exports = {
    makeCartobuy,
    updateCartobuy
}