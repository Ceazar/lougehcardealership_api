const updateCartobuyEntity = ({ }) => {
    return function updateCartobuy({ carid, customernumber, dateofreq, schedule }) {
        if (!carid || !customernumber || !dateofreq || !schedule) {
            throw new Error("Empty fields.");
        }

        return Object.freeze({
            getCarid: () => carid,
            getCustomernumber: () => customernumber,
            getDateofreq: () => dateofreq,
            getSchedule: () => schedule
        })

    }
}

module.exports = updateCartobuyEntity;