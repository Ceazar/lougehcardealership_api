const makeMechanicEntity = ({ }) => {
    return function makeMechanic({ employeeid, serviceticketid, status, priority }) {
        if (!employeeid) {
            throw new Error("Employee ID required.")            
        }
        if (!serviceticketid) {
           serviceticketid = null;
        }
        if (!status) {
            status = "AVAILABLE";
        }
        if (!priority) {
            priority = null;
        }

        return Object.freeze({
            getEmployeeid: () => employeeid,
            getServiceticketid: () => serviceticketid,
            getStatus: () => status,
            getPriority: () => priority
        })
    }
}

module.exports = makeMechanicEntity;