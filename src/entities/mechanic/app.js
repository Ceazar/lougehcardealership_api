const makeMechanicEntity = require('./mechanic');
const updateMechanicEntity = require('./update-mechanic');

const makeMechanic = makeMechanicEntity({});
const updateMechanic = updateMechanicEntity({});

const mechanicEntity = Object.freeze({
    makeMechanic,
    updateMechanic
});

module.exports = mechanicEntity;
module.exports = {
    makeMechanic,
    updateMechanic
}