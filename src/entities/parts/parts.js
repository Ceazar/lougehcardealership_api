const makePartEntity = ({ }) => {
    return function makePart({ partname, description, stock, price }) {
        if (!partname) {
            throw new Error("Partname required.")
        }
        if (!description) {
            description = partname
        }
        if (!stock) {
            throw new Error("Stock required.");
        }
        if (!price) {
            throw new Error("Price is required.")
        }

        return Object.freeze({
            getPartname: () => partname,
            getDescription: () => description,
            getStock: () => stock,
            getPrice: () => price
        })
    }
}

module.exports = makePartEntity