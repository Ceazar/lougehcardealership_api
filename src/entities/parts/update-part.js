const updatePartEntity = ({ }) => {
    return function updatePart({ id, partname, description, stock, price }){
        if (!partname) {
            throw new Error("Partname required.")
        }
        if (!description) {
            description = partname;
        }

        if (!stock) {
            throw new Error("Stock required.")
        }

        if (!price) {
            throw new Error("Price required.")
        }

        return Object.freeze({
            getId: () => id,
            getPartname: () => partname,
            getDescription: () => description,
            getStock: () => stock,
            getPrice: () => price
        })
    }
}

module.exports = updatePartEntity;