const makePartEntity = require('./parts');
const updatePartEntity = require('./update-part');

const makePart = makePartEntity({});
const updatePart = updatePartEntity({});

const partEntity = Object.freeze({
    makePart,
    updatePart
});

module.exports = partEntity;
module.exports = {
    makePart,
    updatePart
}