const updateSalespersonEntity = ({ }) => {
    return function updateSalesperson({ id, employeeid, status, priority }) {
        if (!employeeid) {
            throw new Error("Employee ID required.")            
        }
        if (!status) {
            status = "AVAILABLE";
        }
        if (priority) {
            priority = null;
        }

        return Object.freeze({
            getId: () => id,
            getEmployeeid: () => employeeid,
            getStatus: () => status,
            getPriority: () => priority
        })
    }
}

module.exports = updateSalespersonEntity;