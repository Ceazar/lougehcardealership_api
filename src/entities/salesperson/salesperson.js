const makeSalespersonEntity = ({ }) => {
    return function makeSalesperson({ employeeid, status, priority }) {
        if (!employeeid) {
            throw new Error("Employee ID required.")
        }
        if (!status) {
            status = "AVAILABLE";
        }


        return Object.freeze({
            getEmployeeid: () => employeeid,
            getStatus: () => status,
            getPriority: () => priority
        })
    }
}

module.exports = makeSalespersonEntity;