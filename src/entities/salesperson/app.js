const makeSalespersonEntity = require('./salesperson');
const updateSalespersonEntity = require('./update-salesperson');

const makeSalesperson = makeSalespersonEntity({});
const updateSalesperson = updateSalespersonEntity({});

const mechanicEntity = Object.freeze({
    makeSalesperson,
    updateSalesperson
});

module.exports = mechanicEntity;
module.exports = {
    makeSalesperson,
    updateSalesperson
}