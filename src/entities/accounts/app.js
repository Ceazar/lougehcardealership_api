const makeAccountEntity = require('./accounts');
const updateAccountEntity = require('./update-account');
const loginAccountEntity = require('./login');

const makeAccount = makeAccountEntity({});
const updateAccount = updateAccountEntity({});
const dologinAccount = loginAccountEntity({});

const accountEntity = Object.freeze({
    makeAccount,
    updateAccount,
    dologinAccount
});

module.exports = accountEntity;
module.exports = {
    makeAccount,
    updateAccount,
    dologinAccount
}