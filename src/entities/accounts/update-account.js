const updateAccountEntity = ({ }) => {
    return function updateAccount({ username, password, role }) {


        if (!username || !password) {
            throw new Error("Username and Password required.");
        }
        if (!role) {
            throw new Error("Role required.");
        }

        return Object.freeze({
            getUsername: () => username,
            getPassword: () => password,
            getRole: () => role
        })
    }
}

module.exports = updateAccountEntity;