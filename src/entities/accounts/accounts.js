const makeAccountEntity = ({ }) => {
    return function makeAccount({ userid, username, password, role }) {
        if (!userid) {
            throw new Error("Employee ID/ Customer ID is required.");
        }
        if (!username || !password) {
            throw new Error("Username and Password required.");
        }
        if (password.length < 6) {
            throw new Error("Password must be at least 6 characters.");
        }
        if (!role) {
            throw new Error("Role required. (Customer, Mechanic, Salesperson, Midman, Admin) ")
        }

        return Object.freeze({
            getUserid: () => userid,
            getUsername: () => username,
            getPassword: () => password,
            getRole: () => role
        })
    }
}

module.exports = makeAccountEntity;