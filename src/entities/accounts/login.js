const loginAccountEntity = ({ }) => {
    return function loginAccount({ username, password }) {
        if (!username) {
            throw new Error("Please input username.");
        }
        if (!password) {
            throw new Error("Empty password.")
        }

        return Object.freeze({
            getUsername: () => username,
            getPassword: () => password
        })
    }
}

module.exports = loginAccountEntity;