const makeServiceEntity = ({ }) => {
    return function makeService({ servicename, description, pricerate, duration, mechanics }) {
        if (!servicename) {
            throw new Error("Service Name required.")
        }
        if (!description) {
            description = servicename;
        }
        if (!pricerate) {
            throw new Error("Empty price rate.")
        }

        pricerate = parseFloat(pricerate);

        if (pricerate < 0) {
            throw new Error("Invalid Service pricerate.");
        }

        if (!duration) {
            throw new Error("Empty duration.")
        }

        if (!mechanics) {
            throw new Error("Number of mechanics required.");
        }

        return Object.freeze({
            getServicename: () => servicename,
            getDescription: () => description,
            getPricerate: () => pricerate,
            getDuration: () => duration,
            getMechanics: () => mechanics
        })
    }
}
module.exports = makeServiceEntity;