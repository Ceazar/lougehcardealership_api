const updateServiceEntity = ({ }) => {
    return function updateService({ id, servicename, description, pricerate, duration, mechanics }) {
        if (!servicename) {
            throw new Error("Incomplete fields.")
        }
        if (!description) {
            description = servicename;
        }
        // Check if ID has no decimals and a positive integer
        const decimal = id.indexOf(".") == -1;
        if ((!(Number.isInteger(Number(id)))) || id <= 0 || !decimal) {
            throw new Error("Invalid Service ID.");
        }

        if (!pricerate) {
            throw new Error("Service pricerate required.")
        }

        if ((!(Number.isInteger(Number(pricerate)))) || pricerate <= 0) {
            throw new Error("Invalid Price rate.");
        }

        if (!duration) {
            throw new Error("Empty duration.");
        }

        if (!mechanics) {
            throw new Error("Number of mechanics required.");
        }

        return Object.freeze({
            getId: () => id,
            getServicename: () => servicename,
            getDescription: () => description,
            getPricerate: () => pricerate,
            getDuration: () => duration,
            getMechanics: () => mechanics
        })
    }
}

module.exports = updateServiceEntity;