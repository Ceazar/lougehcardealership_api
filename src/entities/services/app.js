const makeServiceEntity = require('./services');
const updateServiceEntity = require('./update-service');
const uploadServiceEntity = require('./upload-service');

const makeService = makeServiceEntity({});
const updateService = updateServiceEntity({});
const uploadService = uploadServiceEntity({});

const serviceEntity = Object.freeze({
    makeService,
    updateService,
    uploadService
});

module.exports = serviceEntity;
module.exports = {
    makeService,
    updateService,
    uploadService
}