const uploadServiceEntity = ({ }) => {
    return function uploadService({filename, id}) {
        //  console.log(filename, id);
        if (!filename) {
            throw new Error("Invalid File.");
        }
        if (!id) {
            throw new Error("Service ID required.");
        }

        return Object.freeze({
            getServiceid: () => id,
            getImage: () => filename
        })
    }
}

module.exports = uploadServiceEntity;