const makeEmployeeEntity = ({ }) => {
    return function makeEmployee({ employeeid, fname, mi, lname, position, age, phone, email, username, password }) {
        if (!employeeid) {
            throw new Error("Employee id required.");
        }
        if (!fname || !lname) {
            throw new Error("Incomplete name.");
        }
        if (!mi) {
            mi = "";
        }
        if (!position) {
            throw new Error("Position required.");
        }
        if (!age) {
            throw new Error("Age required.");
        }
        const stringAge = toString(age);
        const agechecker = stringAge.indexOf(".") == -1;
        if ((!(Number.isInteger(Number(age)))) || age <= 0 || !agechecker) {
            throw new Error("Invalid Age.");
        }
        if (!phone) {
            phone = "n/a";
        }
        if (!email) {
            email = "n/a";
        }


        return Object.freeze({
            getEmployeeId: () => employeeid,
            getFname: () => fname,
            getMi: () => mi,
            getLname: () => lname,
            getPosition: () => position,
            getAge: () => age,
            getPhone: () => phone,
            getEmail: () => email,
            getUsername: () => username,
            getPassword: () => password,
        });

    }
}

module.exports = makeEmployeeEntity;