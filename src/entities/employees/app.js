const makeEmployeeEntity = require('./employee');
const makeupdateEmployeeEntity = require('./update-employees');

const makeEmployee = makeEmployeeEntity({});
const makeupdateEmployee = makeupdateEmployeeEntity({});

const employeeEntity = Object.freeze({
    makeEmployee,
    makeupdateEmployee
});

module.exports = employeeEntity;
module.exports = {
    makeEmployee,
    makeupdateEmployee
}