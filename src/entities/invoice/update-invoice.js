const updateInvoiceEntity = ({ }) => {
    return function updateInvoice({
        id,
        customernumber,
        carid,
        serviceticketid,
        date,
        type,
        amount
    }) {
        if (!customernumber) {
            throw new Error("Customer ID required.")
        }
        if (!carid) {
            carid = null;
            type = "Service invoice"

            if (!serviceticketid) {
                throw new Error("Service ticket ID required.")
            }
        }
        else {
            type = "Sales invoice"
        }
        if (!date) {
            date = String(new Date());
        }

        if (!amount) {
            throw new Error("Total amount required.")
        }
        if (amount < 0) {
            throw new Error("Invalid total amount.")
        }

        return Object.freeze({
            getId: () => id,
            getCustomernumber: () => customernumber,
            getCarid: () => carid,
            getServiceticketid: () => serviceticketid,
            getDate: () => date,
            getType: () => type,
            getTotalamount: () => totalamount
        })

    }
}

module.exports = updateInvoiceEntity;