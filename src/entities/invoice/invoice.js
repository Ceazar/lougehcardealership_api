const makeInvoiceEntity = ({ }) => {
    return function makeInvoice({
        customernumber,
        carid,
        serviceticketnumber,
        date,
        type
    }) {
        if (!customernumber) {
            throw new Error("Customer ID required.")
        }
        if (carid && serviceticketnumber) {
            throw new Error("Please only input either carid(Service Invoice) or serviceticketnumber(Serviceticket Invoice).")
        }
        if (!carid) {
            carid = null;
            type = "Service invoice"

            if (!serviceticketnumber) {
                throw new Error("Service ticket Number required.")
            }
        } else {
            type = "Sales invoice"
        }
        if (!serviceticketnumber) {
            serviceticketnumber = null;
        }
        if (!date) {
            date = String(new Date());
        }

        return Object.freeze({
            getCustomernumber: () => customernumber,
            getCarid: () => carid,
            getServiceticketnumber: () => serviceticketnumber,
            getDate: () => date,
            getType: () => type
        })

    }
}

module.exports = makeInvoiceEntity;