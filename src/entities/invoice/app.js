const makeInvoiceEntity = require('./invoice');
const updateInvoiceEntity = require('./update-invoice');

const makeInvoice = makeInvoiceEntity({});
const updateInvoice = updateInvoiceEntity({});

const invoiceEntity = Object.freeze({
    makeInvoice,
    updateInvoice
});

module.exports = invoiceEntity;
module.exports = {
    makeInvoice,
    updateInvoice
}