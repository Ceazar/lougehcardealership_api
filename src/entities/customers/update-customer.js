const updateCustomerEntity = ({}) => {
    return function updateCustomer({ id, fname, mi, lname, email, phone }) {
        if (!fname || !lname) {
            //console.log(fname, mi, lname);
            throw new Error("Incomplete name.");
        }
        if (!mi) {
            mi = "";
        }
        if (!phone) {
            phone = "n/a";
        }
        if (!email) {
            throw new Error("Email required.");
        }

        return Object.freeze({
            getId: () => id,
            getFname: () => fname,
            getMi: () => mi,
            getLname: () => lname,
            getEmail: () => email,
            getPhone: () => phone
        });

    }
}

module.exports = updateCustomerEntity;