const makeCustomerEntity = require('./customers');
const updateCustomerEntity = require('./update-customer');

const makeCustomer = makeCustomerEntity({});
const updateCustomer = updateCustomerEntity({});

const customerEntity = Object.freeze({
    makeCustomer,
    updateCustomer
});

module.exports = customerEntity;
module.exports = {
    makeCustomer,
    updateCustomer
}