const makeCustomerEntity = ({ }) => {
    return function makeCustomer({ customernumber, fname, mi, lname, email, phone }) {
        if (!fname || !lname) {
            throw new Error("Name required.");
        }
        if (!mi) {
            mi = "";
        }
        if (!email) {
            throw new Error("Email required.");
        }
        if (!phone) {
            phone = "N/A";
        }

        customernumber = Date.now();

        return Object.freeze({
            getCustomernum: () => customernumber,
            getFname: () => fname,
            getMi: () => mi,
            getLname: () => lname,
            getEmail: () => email,
            getPhone: () => phone
        })
    }
}

module.exports = makeCustomerEntity;