const makeCarEntity = ({ }) => {
    return function makeCar({ vin, model, company, color, status, price, available }) {
        if (!vin) {
            throw new Error("VIN Required.");
        }
        if (!model) {
            throw new Error("Car model required.");
        }
        if (!company) {
            throw new Error("Company required.");
        }
        if (!color) {
            throw new Error("Car color required.");
        }
        if (!status) {
            throw new Error("Car status required.");
        }
        if (!price || isNaN(price)) {
            throw new Error("Car price required.");
        }
        if (!available) {
            available = true;
        }

        return Object.freeze({
            getVin: () => vin,
            getModel: () => model,
            getCompany: () => company,
            getColor: () => color,
            getStatus: () => status,
            getPrice: () => price,
            getAvailabile: () => available
        });

    }
}

module.exports = makeCarEntity;