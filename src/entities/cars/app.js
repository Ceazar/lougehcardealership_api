const makeCarEntity = require('./cars');
const updateCarEntity = require('./update-car');
const uploadCarEntity = require('./upload-car');

const makeCar = makeCarEntity({});
const updateCar = updateCarEntity({});
const uploadCar = uploadCarEntity({});

const carEntity = Object.freeze({
    makeCar,
    updateCar,
    uploadCar
});

module.exports = carEntity;
module.exports = {
    makeCar,
    updateCar,
    uploadCar
}