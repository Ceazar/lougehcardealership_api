const uploadCarEntity = ({ }) => {
    return function uploadCar({filename, id}) {
        //  console.log(filename, id);
        if (!filename) {
            throw new Error("Invalid File.");
        }
        if (!id) {
            throw new Error("Car ID required.");
        }

        return Object.freeze({
            getCarid: () => id,
            getImage: () => filename
        })
    }
}

module.exports = uploadCarEntity;